FROM node:20.12-alpine3.19 as builder
WORKDIR /app

COPY ./.nginx /app/.nginx
COPY ./public /app/public
COPY ./src /app/src
COPY ./package.json /app/package.json
COPY ./tsconfig.json /app/tsconfig.json
COPY .env /app/.env
COPY node_modules /app/node_modules

RUN npm run build


FROM nginx:alpine

COPY .nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/build /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]
