import React from 'react';

import { useUserContext } from '../context/userContext';

import OfferToSignIn from '../pages/OfferToSignIn/OfferToSignIn';


export const withOfferToSignIn = (WrappedComponent: any) => {
    return () => {
        const { user } = useUserContext()
        return (
            user.Empty ?
                <OfferToSignIn />
                :
                <WrappedComponent />
        );
    };
}
