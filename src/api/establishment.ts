import {API_URL} from "../utils/Constants"
import {encodeURISearchParams} from "../utils/Functions";


export const getEstablishment = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}`, {
        method: 'GET'
    })
}

export const getEstablishmentEvents = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/events`, {
        method: 'GET'
    })
}

export const getEstablishmentPromotions = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/promotions`, {
        method: 'GET'
    })
}

export const searchEstablishments = async (page: number, params?: object) => {
    return fetch(API_URL + `/establishments/search?page=${page}${params ? encodeURISearchParams(params) : ""}`, {
        method: 'GET'
    })
}

export const getInputVars = async () => {
    return fetch(API_URL + `/establishments/inputvars`, {
        method: 'GET'
    })
}

export const searchAddress = async (address: string) => {
    return fetch(API_URL + `/establishments/inputaddress?fulltextfind=${address}`, {
        method: 'GET'
    })
}

export const getMenu = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/menu`, {
        method: 'GET'
    })
}

export type RestType = {
    EstablishmentGUID?: string
    EstablishmentTitle: string
    EstablishmentLogo: string
    Address: string
    AvgBill: number
    Type: string
    Cuisines: string[]
    Description: string
    Images: string[]
    Contacts: {
        MobilePhone: string
        SiteURL: string
        Telegram: string
        VK: string
    },
    AdditionalServices: string[]
    SuitableCases: string[]
    Schedule: {
        Monday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Tuesday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Wednesday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Thursday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Friday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Saturday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        },
        Sunday: {
            OpenTime: number
            CloseTime: number
            RestDay: boolean
        }
    }
}

export const createUpdateRest = async (body: RestType, AuthorizationToken: string) => {
    return fetch(API_URL + '/establishments/create', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const createUpdateMeal = async (body: {
    Category: string,
    Price: number,
    Weight: number,
    Volume: number,
    DishImage: string,
    DishName: string,
    Composition: string,
    Calories: number,
    Proteins: number,
    Fats: number,
    Carbohydrates: number,
    DishGUID?: string
}, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/menu/create`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const createUpdatePromotion = async (body: {
    Title: string;
    Image: string;
    Conditions: string;
    StartTime: number;
    EndTime: number;
    Undying: boolean;
    ContactInfo: string;
    PromotionGUID?: string
}, EstablishmentGUID: string | undefined, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/promotions/create`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const deletePromotion = async (PromotionGUID: string, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/promotions/${PromotionGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const createUpdateEvent = async (body: {
    Title: string;
    Image: string;
    Description: string;
    DateTime: number;
    ContactInfo: string;
    PromotionGUID?: string
}, EstablishmentGUID: string | undefined, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/events/create`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const deleteEvent = async (EventGUID: string, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/events/${EventGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const deleteMeal = async (EstablishmentGUID: string, DishGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/menu/${DishGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const getMyEstablishments = async (AuthorizationToken: string, AccountGUID: string) => {
    return fetch(API_URL + `/establishments/${AccountGUID}/getall`, {
        method: 'GET',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const uploadImage = async (formData: FormData, AuthorizationToken: string) => {
    return fetch(API_URL + '/establishments/uploadImage', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: formData
    })
}

export type NewReview = {
    LikedTheMost: string
    NeedToBeChanged: string
    Comment: string
    Rating: {
        Service: number
        Food: number
        Vibe: number
        PriceQuality: number
        WaitingTime: number
    },
    Images: string[]
}

export const getUserReviews = async (ReviewerGUID: string) => {
    return fetch(API_URL + `/establishments/reviewers/${ReviewerGUID}/reviews`, {
        method: 'GET'
    })
}

export const getEstablishmentReviews = async (EstablishmentGUID: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/reviews`, {
        method: 'GET'
    })
}

export const createReview = async (body: NewReview, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/${EstablishmentGUID}/review/create`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const deleteReview = async (ReviewGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/reviews/${ReviewGUID}`,
    {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}

export const getEvents = async (City: string) => {
    return fetch(API_URL + `/establishments/events/bycity?city=${encodeURI(City)}`, {
        method: 'GET',
    })
}

export const getCollections = async (AccountGUID: string) => {
    return fetch(API_URL + `/establishments/users/${AccountGUID}/collections`, {
        method: 'GET'
    })
}

export const getCollection = async (CollectionGUID: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}`, {
        method: 'GET'
    })
}

export const createCollection = async (body: { Title: string, CollectionGUID?: string }, AuthorizationToken: string) => {
    return fetch(API_URL + '/establishments/collections/create', {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const addToCollection = async (body: { EstablishmentGUID: string }, CollectionGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}/add`, {
        method: 'POST',
        headers: {
            'Authorization': AuthorizationToken
        },
        body: JSON.stringify(body)
    })
}

export const removeFromCollection = async (CollectionGUID: string, EstablishmentGUID: string, AuthorizationToken: string) => {
    return fetch(API_URL + `/establishments/collections/${CollectionGUID}/delete/${EstablishmentGUID}`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthorizationToken
        }
    })
}
