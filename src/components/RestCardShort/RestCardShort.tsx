import React from "react";
import { Link } from "react-router-dom";

import { iRestCardProps } from "../RestCard/RestCard";

import Stars from "../UIKit/Stars/Stars";
import Icons from "../UIKit/Icons";

import styles from "./RestCardShort.module.css";
import {switchEnding} from "../../utils/Functions";
import AddToCollection from "../AddToCollection/AddToCollection";
import {useUserContext} from "../../context/userContext";


const RestCardShort: React.FC<iRestCardProps> = (props) => {
    const {user} = useUserContext()
    return (
        <div className={styles.card}>
            <div className={styles.photo}>
                <Link to={`/rest/${props.EstablishmentGUID}`} className={styles.title} target={'_blank'}><img src={props.Image.length > 0 ? props.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg'} alt={''} /></Link>
                {
                    !user.Empty ?
                        <div className={styles.heart_container}>
                            <AddToCollection EstablishmentCollections={props.CollectionGUIDs}
                                             EstablishmentGUID={props.EstablishmentGUID}/>
                        </div>
                        :
                        <></>
                }
            </div>
            <div className={styles.info}>
                <Link to={`/rest/${props.EstablishmentGUID}`} className={styles.title}
                      target={'_blank'}>{props.EstablishmentTitle}</Link>
                <div className={styles.rating}>
                    <Stars rating={props.AvgRating} size={"s18"} />
                    <span>{props.AvgRating.toFixed(1)}</span>
                    <span className={styles.evals}>({props.ReviewCount} {switchEnding(['оценок', "оценка", "оценок"], props.ReviewCount)})</span>
                </div>
                <p className={styles.price}>Средний чек ~{props.AvgBill} ₽</p>
            </div>

        </div>
    )
}

export default RestCardShort;
