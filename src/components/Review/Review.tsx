import React, {useState} from "react";

import styles from "./Review.module.css";
import Icons from "../UIKit/Icons";
import RatingCriterion from "../RatingCriterion/RatingCriterion";

import {numberToDate, switchCriterion} from "../../utils/Functions";
import {deletePromotion, deleteReview} from "../../api/establishment";
import {executionAsyncResource} from "node:async_hooks";
import {useUserContext} from "../../context/userContext";

interface iReview {
    ReviewGUID: string
    Establishment: {
        EstablishmentGUID: string
        EstablishmentImage: string
        EstablishmentTitle: string
    },
    Rating: {
        Service: number
        Food: number
        Vibe: number
        WaitingTime: number
        PriceQuality: number
    },
    Comment: string
    LikedTheMost: string
    NeedToBeChanged: string
    Images: string[]
    Timestamp: number
    User: {
        AccountGUID: string
        Name: string
        Surname: string
        UserImage: string
    }
}

interface iReviewProps {
    type: 'user' | 'rest',
    review: iReview,
    onDelete: () => void
}


export interface iReviews {
    Count: number,
    Reviews: iReview[]
}


const Review = (props: iReviewProps) => {

    const {user} = useUserContext();

    const deleteFn = async () => {
        await deleteReview(props.review.ReviewGUID, user.At.Token).then(resp => {
            console.log(resp)
            if (resp.ok) {
                props.onDelete()
            }
        })
    }

    return (
        <div className={styles.avatar_review}>
            {props.type === 'user' ?
                <img src={props.review.Establishment.EstablishmentImage} className={styles.avatar}/>
                :
                props.review.User.UserImage ?
                    <img src={props.review.User.UserImage} className={styles.avatar}/>
                    :
                    <div className={styles.avatar}>{props.review.User.Name[0]}{props.review.User.Surname[0]}</div>
            }

            <div className={styles.review}>
                <div className={styles.user_report}>
                    <div>
                    <div
                            className={styles.title}>{props.type === 'user' ? props.review.Establishment.EstablishmentTitle : props.review.User.Name + ' ' + props.review.User.Surname}</div>
                        <div className={styles.date}>{numberToDate(props.review.Timestamp * 1000)}</div>
                    </div>
                    {props.type === 'user' && <Icons icon={'delete'} onClick={() => deleteFn()}/>}
                </div>

                <div className={styles.rating_criteria}>
                    {Object.keys(props.review.Rating).map((criterion, index) =>
                        // @ts-ignore
                        <RatingCriterion criterion={switchCriterion(criterion.toLowerCase())} rating={props.review.Rating[criterion]} key={index} />)}
                </div>

                <div className={styles.text}>
                    <p><span className={styles.subtitle}>Понравилось:</span> {props.review.LikedTheMost} </p>
                    <p><span className={styles.subtitle}>Не понравилось:</span> {props.review.NeedToBeChanged} </p>
                    <p><span className={styles.subtitle}>Комментарий:</span> {props.review.Comment} </p>
                </div>

                <div className={styles.photos}>
                    {props.review.Images.map((i, index) => <img src={i} className={styles.photo}/> )}
                </div>
            </div>
        </div>
    )
}

export default Review;