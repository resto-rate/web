import React from 'react';
import styles from './EmptyStates.module.css';

import notFound from './imgs/NoSearchResult.svg';
import empty from './imgs/EmptyInbox.svg';

interface IEmptyStatesProps {
    type: 'empty' | 'not found',
    heading: string,
    text: string
}

const EmptyStates: React.FC<IEmptyStatesProps> = (props) => {
    return (
        <div className={styles.container}>
            {props.type === 'not found' ? <img src={notFound} alt={'not found'}/> : <img src={empty} alt={'empty'}/>}
            <h1>{props.heading}</h1>
            <p>{props.text}</p>
        </div>
    )
}

export default EmptyStates;