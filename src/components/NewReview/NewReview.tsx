import React, {useEffect, useState} from 'react';
import styles from './NewReview.module.css';
import StarsInput from "../UIKit/StarsInput/StarsInput";
import Input from "../UIKit/Input/Input";
import Icons from "../UIKit/Icons";
import Button from "../UIKit/Button/Button";
import Stars from "../UIKit/Stars/Stars";
import {useUserContext} from "../../context/userContext";
import {useParams, useSearchParams} from "react-router-dom";
import {createReview, getEstablishment, uploadImage} from "../../api/establishment";
import {iRest} from "../../pages/Rest/Rest";
import {switchEnding} from "../../utils/Functions";
import {useAlertContext} from "../../context/alertContext";


export type NewReview = {
    LikedTheMost: string
    NeedToBeChanged: string
    Comment: string
    Rating: {
        Service: number
        Food: number
        Vibe: number
        PriceQuality: number
        WaitingTime: number
    },
    Images: string[]
}


const NewReview = (props: { onComplete: () => void }) => {
    const {setAlert} = useAlertContext()
    const {user} = useUserContext()
    const params = useParams()
    const [searchParams] = useSearchParams()
    const establishmentId = params.id ? params.id : searchParams.get('id')

    const [service, setService] = useState<number>(0);
    const [vibe, setVibe] = useState<number>(0);
    const [food, setFood] = useState<number>(0);
    const [waitingTime, setWaitingTime] = useState<number>(0);
    const [priceQuality, setPriceQuality] = useState<number>(0);

    const [likeText, setLikeText] = useState<string>("");
    const [dislikeText, setDislikeText] = useState<string>("");
    const [comment, setComment] = useState<string>("");

    const [photos, setPhotos] = useState<string[]>([])
    const [rest, setRest] = useState<iRest | null>(null)


    const upload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            console.log("Uploading file...");

            const formData = new FormData();
            // @ts-ignore
            formData.append("imageFile", e.target.files[0]);
            let result = null
            try {
                result = await uploadImage(formData, user.At.Token)
            } catch (error) {
                console.log(error)
                setAlert({
                    icon: "attention",
                    message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                });
            }
            if (result === null)
                return
            if (result.ok) {
                const data = await result.json();
                setPhotos(photos.concat([data]))
                //@ts-ignore
                document.getElementById('photo_input').value = ''
            } else {
                switch (result.status) {
                    case 413:
                        setAlert({
                            icon: "attention",
                            message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                        });
                }
            }
        }
    }

    const removePhoto = (ind: number) => {
        const newPhotos = structuredClone(photos)
        newPhotos.splice(ind, 1)
        setPhotos(newPhotos)
    };

    const sendReview = async () => {
        if (establishmentId) {
            const body: NewReview = {
                LikedTheMost: likeText,
                NeedToBeChanged: dislikeText,
                Comment: comment,
                Rating: {
                    Service: service,
                    Food: food,
                    Vibe: vibe,
                    PriceQuality: priceQuality,
                    WaitingTime: waitingTime
                },
                Images: photos
            }

            if (service === 0 || food === 0 || vibe === 0 || priceQuality === 0 || waitingTime === 0) {
                return
            }

            await createReview(body, establishmentId, user.At.Token).then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then(body => {
                console.log(body)
                props.onComplete()
            })
        }
    }

    const fetchEstablishment = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                setRest(body)
            } else {
                console.log('Wrong establishment id')
            }
        })
    }

    const uploadPhoto = async () => {
        document.getElementById('photo_input')?.click()
    }

    useEffect(() => {
        fetchEstablishment()
    }, [])

    return (
        <div className={styles.container}>
            <input type="file" id="photo_input" onChange={upload} hidden/>
            <h1>Поделитесь впечатлениями</h1>
            {
                rest !== null ?
                    <div className={styles.avatar_rest}>
                        <img src={rest.EstablishmentLogo} className={styles.avatar} alt="rest"/>
                        <div className={styles.rest}>
                            <h2>{rest.EstablishmentTitle}</h2>
                            <div className={styles.rating}>
                                <Stars rating={rest.Rating.AvgRating} size={'s18'}/>
                                <span>{rest.Rating.AvgRating}</span>
                                <p>({rest.Rating.ReviewCount} {switchEnding(['отзывов', 'отзыв', 'отзывов'], rest.Rating.ReviewCount)})</p>
                            </div>

                        </div>
                    </div>
                    :
                    <></>
            }


            <div className={styles.title_rates}>
                <h2>Оценка по критериям</h2>
                <div className={styles.rates}>
                    <StarsInput criterion={'Обслуживание'} value={service} setValue={setService}/>
                    <StarsInput criterion={'Атмосфера'} value={vibe} setValue={setVibe}/>
                    <StarsInput criterion={'Питание'} value={food} setValue={setFood}/>
                    <StarsInput criterion={'Время ожидания'} value={waitingTime} setValue={setWaitingTime}/>
                    <StarsInput criterion={'Цена-качество'} value={priceQuality} setValue={setPriceQuality}/>
                </div>
            </div>

            <div className={styles.like_dislike}>
                <div className={styles.text_review}>
                    <h2>Что понравилось?</h2>
                    <Input type={"textarea"} value={likeText} setValue={setLikeText}/>
                </div>

                <div className={styles.text_review}>
                    <h2>Что стоит изменить?</h2>
                    <Input type={"textarea"} value={dislikeText} setValue={setDislikeText}/>
                </div>
            </div>

            <div className={styles.text_review}>
                <div className={styles.comment}>
                    <h3>Комментарий</h3>
                    <p>до 1000 символов</p>
                </div>
                <Input type={"textarea"} value={comment} setValue={setComment}/>
            </div>

            <div className={styles.photos_container}>
                {
                    photos.map((el, ind) =>
                        <div
                            key={ind}
                            className={styles.image_array_element}
                        >
                            <img src={el}
                                 style={{width: 80, height: 80, borderRadius: 8, objectFit: 'cover', cursor: 'grab'}}/>
                            <Icons icon={'cross'} onClick={() => removePhoto(ind)}/>
                        </div>
                    )
                }
                {
                    photos.length < 20 ?
                        <Button type={'secondary'} size={'M'} onClick={uploadPhoto}><Icons icon={'add-photo'}
                                                                                           className={styles.photo_icon}/></Button>
                        :
                        <></>
                }
            </div>

            <Button type={"primary"} size={"L"} onClick={sendReview}>Оставить отзыв</Button>

        </div>
    )
}

export default NewReview;