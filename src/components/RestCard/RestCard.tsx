import React from "react";
import { Col, Row } from 'react-bootstrap';
import { Link, useLocation } from "react-router-dom";

import Chips from "../UIKit/Chips/Chips";
import Icons from "../UIKit/Icons";
import Stars from "../UIKit/Stars/Stars";
import Button from "../UIKit/Button/Button";

import styles from "./RestCard.module.css";
import {switchEnding} from "../../utils/Functions";
import AddToCollection from "../AddToCollection/AddToCollection";
import {useUserContext} from "../../context/userContext";


export interface iRestCardProps {
    AvgBill: number
    AvgRating: number
    Cuisines: string[]
    CollectionGUIDs: string[]
    EstablishmentGUID: string
    EstablishmentTitle: string
    Image: string
    ReviewCount: number
    Type: string
}

const RestCard: React.FC<iRestCardProps> = (props) => {
    const location = useLocation()
    const {user} = useUserContext()
    return (
        <Row className={styles.card}>
            <Col md={3} className={styles.photo}>
                <Link to={`/${location.pathname === '/my-rests' ? 'my-rests' : 'rest'}/${props.EstablishmentGUID}`} target={location.pathname === '/my-rests' ? '_self' : '_blank'}><img src={props.Image.length > 0 ? props.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg'} alt='' /></Link>
            </Col>
            <Col md={9} className={styles.info}>
                <div className={styles.type_heart}>
                    <div><Chips color={"dark"} label={props.Type} /></div>
                    {
                        !user.Empty ?
                            <div className={styles.heart_container}>
                                <AddToCollection EstablishmentCollections={props.CollectionGUIDs}
                                                 EstablishmentGUID={props.EstablishmentGUID}/>
                            </div>
                            :
                            <></>
                    }
                </div>
                <div className={styles.title_rating}>
                    <Link to={`/${location.pathname === '/my-rests' ? 'my-rests' : 'rest'}/${props.EstablishmentGUID}`} className={styles.title} target={location.pathname === '/my-rests' ? '_self' : '_blank'}>{props.EstablishmentTitle}</Link>
                    <div className={styles.rating_price}>
                        <div className={styles.rating}>
                            <Stars rating={props.AvgRating} size={"s18"} />
                            <span>{props.AvgRating.toFixed(1)}</span>
                            <span className={styles.evals}>({props.ReviewCount} {switchEnding(['оценок', "оценка", "оценок"], props.ReviewCount)})</span>
                        </div>
                        <p className={styles.price}>Средний чек ~{props.AvgBill} ₽</p>
                    </div>
                </div>
                <div className={styles.cuisines_menu}>
                    <div className={styles.cuisines}>
                        {
                            props.Cuisines.map((el, ind) => <Chips label={el} color={"light"} />)
                        }
                    </div>
                    <Button type={"text"} size={"M"} onClick={() => 0}>Посмотреть меню</Button>
                </div>
            </Col>
        </Row>
    )
}

export default RestCard;