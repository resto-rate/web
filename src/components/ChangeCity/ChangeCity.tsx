import React, {useContext, useEffect, useState} from "react";
import styles from "./ChangeCity.module.css";
import {changeProfileData, getCities} from "../../api/auth";
import {useUserContext} from "../../context/userContext";


const ChangeCity = () => {
    const [cities, setCities] = useState<string[]>([])

    const {user, setUser} = useUserContext();

    const fetchCities = async () => {
        await getCities().then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setCities(body.Cities.sort())
                console.log(body.Cities)
            }
        })
    }

    useEffect(() => {
        fetchCities()
    }, [])

    const updateCity = async (newCity: string) => {
        localStorage.setItem('city', newCity)
        if (user.Empty) {
            setUser({...user, City: newCity})
            return
        }
        await changeProfileData({City: newCity}, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            setUser({...user, ...body, Empty: false})
            localStorage.setItem('user', JSON.stringify({...user, ...body, Empty: false}))
        })
    }

    return (
        <div className={styles.container}>
            <h1>Города для поиска мест</h1>
            <div className={styles.cities}>
                {
                    cities.map((element, index) =>
                        <p key={index} onClick={() => updateCity(element)} style={{color: element === user.City ? 'var(--text-active)' : 'var(--text-primary)'}}>{element}</p>)
                }
            </div>
        </div>
    )
}

export default ChangeCity;
