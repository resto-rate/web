import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import Stars from '../../../components/UIKit/Stars/Stars'
import Button from '../../../components/UIKit/Button/Button'
import Chips from '../../../components/UIKit/Chips/Chips'

import styles from './RestHeader.module.css'
import Icons from '../../../components/UIKit/Icons'
import { Link, useLocation, useParams, useSearchParams } from 'react-router-dom'
import { getEstablishment } from '../../../api/establishment'
import { iRest } from '../../../pages/Rest/Rest'
import {copyCurrentHref, prettyPhone, switchEnding} from '../../../utils/Functions'
import { useAlertContext } from "../../../context/alertContext";


const RestHeader = () => {
    const [rest, setRest] = useState<iRest | null>(null)
    const [searchParams] = useSearchParams()
    const params = useParams()
    const location = useLocation()
    const { setAlert } = useAlertContext();

    const copyAndAlert = () => {
        copyCurrentHref()
        setAlert({
            icon: "share",
            message: "Ссылка на заведение скопирована в буфер обмена. Поделитесь с друзьями местом"
        })
    }

    const establishmentId = params.id ? params.id : searchParams.get('id')

    const fetchEstablishment = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setRest({ ...body })
            } else {
                console.log('Wrong establishment id')
            }
            console.log(rest)
        })
    }

    useEffect(() => {
        fetchEstablishment()
    }, [])

    if (!rest) {
        return <div>Загрузка...</div>
    } else
        return (
            <div className={styles.container}>
                <div className={styles.background} />
                <div className={styles.content}>
                    <Row style={{ alignItems: 'center' }}>
                        <Col xs={1}>
                            <Link to={`/${location.pathname.substring(0, 9) === '/my-rests' ? 'my-rests' : 'rest'}/${establishmentId}`}><img className={styles.logo} src={rest.EstablishmentLogo} alt={''} /></Link>
                        </Col>
                        <Col xs={8}>
                            <div>
                                <Link to={`/${location.pathname.substring(0, 9) === '/my-rests' ? 'my-rests' : 'rest'}/${establishmentId}`}><h1 className={styles.title}>{rest.EstablishmentTitle}</h1></Link>
                                <div className={styles.rating_container}>
                                    <Stars rating={rest.Rating.AvgRating} size={'m'} />
                                    <span className={styles.rating}>{rest.Rating.AvgRating.toFixed(1)}</span>
                                    <span className={styles.reviews}>({ rest.Rating.ReviewCount } { switchEnding(['оценок', 'оценка', 'оценок'], rest.Rating.ReviewCount)})</span>
                                </div>
                            </div>
                        </Col>
                        <Col xs={3}>
                            <div className={styles.buttons}>
                                <Button type='secondary' size='S' onClick={copyAndAlert}>Поделиться</Button>
                                <Button type='primary' size='S' onClick={() => { }} disabled>Сохранить</Button>
                            </div>
                            <div className={styles.schedule_tel_container}>
                                <span>Открыто до 23:00</span>
                                <span>{prettyPhone(rest.Contacts.MobilePhone)}</span>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1} />
                        <Col xs={8}>
                            <div className={styles.chips}>
                                <Chips color='dark' label={rest.Type} />
                                {
                                    rest.Cuisines.map((kitchen, index) => <Chips key={index} color='light' label={kitchen} />)
                                }
                            </div>
                        </Col>
                        <Col xs={3}>
                            <div className={styles.social_media}>
                                {
                                    rest.Contacts.VK ?
                                        <Link to={rest.Contacts.VK} target={'_blank'}><Icons icon={'vk'} /></Link>
                                        : <></>

                                }
                                {
                                    rest.Contacts.WhatsApp ?
                                        <Link to={rest.Contacts.WhatsApp} target={'_blank'}><Icons icon={'whatsapp'} /></Link>
                                        : <></>

                                }
                                {
                                    rest.Contacts.Telegram ?
                                        <Link to={rest.Contacts.Telegram} target={'_blank'}><Icons icon={'telegram'} /></Link>
                                        : <></>

                                }
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
}

export default RestHeader
