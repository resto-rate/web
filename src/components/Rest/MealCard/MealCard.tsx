import React from 'react'
import styles from './MealCard.module.css'
import Chips from '../../UIKit/Chips/Chips'
import { iRest } from '../../../pages/Rest/Rest'
import {switchEnding} from "../../../utils/Functions";


export interface iMeal {
    Calories: number
    Carbohydrates: number
    Category: string
    Composition: string
    DishGUID: string
    DishImage: string
    DishName: string
    EstablishmentGUID: string
    Fats: number
    LikeCount: number
    Price: number
    Proteins: number
    Volume: number
    Weight: number
}

interface iMealProps {
    meal: iMeal
    style?: any
}

const MealCard = (props: iMealProps) => {
    return (
        <div className={styles.container}>
            <div style={{ position: 'relative' }}>
                <img className={styles.preview} src={props.meal.DishImage.length > 0 ? props.meal.DishImage : 'https://restorate.hb.ru-msk.vkcs.cloud/static/preview.jpg'} />
                <Chips label={`Понравилось ${props.meal.LikeCount} ${switchEnding(['посетителям', 'посетителю', 'посетителям'], props.meal.LikeCount)}`} color={'dark'} className={styles.chips} />
            </div>
            <div className={styles.text}>
                <div className={styles.bg} />
                <p className={styles.title}>{props.meal.DishName}</p>
                {
                    props.meal.Composition ?
                        <span className={styles.composition}>Состав: {props.meal.Composition}</span>
                        : <></>
                }
                {
                    props.meal.Calories ?
                        <div className={styles.cpfc}>
                            <span>К/Б/Ж/У</span>
                            <span>{props.meal.Calories}/{props.meal.Proteins}/{props.meal.Fats}/{props.meal.Carbohydrates}</span>
                        </div>
                        : <></>
                }
                <div className={styles.info}>
                    <p>{props.meal.Price} ₽</p>
                    <p>{props.meal.Weight == 0 ? props.meal.Volume + ' мл' : props.meal.Weight + ' г'}</p>
                </div>
            </div>
        </div>
    )
}

export default MealCard
