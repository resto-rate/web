import React, {useEffect, useRef} from "react";
import {createPortal} from 'react-dom';

import styles from './Modal.module.css';
import Icons from "../UIKit/Icons";


const Modal: React.FC<{ children: React.ReactNode, open: boolean, setOpen: Function }> = (props) => {
    const dialog = useRef<HTMLDialogElement>(null)

    useEffect(() => {
        if (props.open) {
            // @ts-ignore
            dialog.current.showModal()
        } else {
            // @ts-ignore
            dialog.current.close()
        }
    }, [props.open]);

    return createPortal(
        <dialog ref={dialog} className={styles.modal}>
            {props.children}
            <Icons icon={"cross"} onClick={() => props.setOpen(false)} className={styles.cross} id={'modal-cross'}/>
        </dialog>,
        document.getElementById('modal')!
    )
}

export default Modal;
