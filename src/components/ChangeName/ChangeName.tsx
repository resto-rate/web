import React, {useState} from "react";
import {Simulate} from "react-dom/test-utils";

import styles from "./ChangeName.module.css";
import Input from "../UIKit/Input/Input";
import Button from "../UIKit/Button/Button";
import {useUserContext} from "../../context/userContext";
import {changeProfileData} from "../../api/auth";
import {capitalizeFirst} from "../../utils/Functions";


const ChangeName: React.FC = () => {
    const { user, setUser } = useUserContext();
    const [firstName, setFirstName] = useState(user.Name);
    const [lastName, setLastName] = useState(user.Surname);
    const [isNameLoading, setIsNameLoading] = useState(false);

    const sendNewUserData = async () => {
        if (!firstName || !lastName) {
            console.log('Имя и фамилия не могут быть пустыми!')
            return false
        }
        return changeProfileData({ Name: capitalizeFirst(firstName.replace(/\s/g, "")), Surname: capitalizeFirst(lastName.replace(/\s/g, "")) }, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            return resp.json()
        }).then(body => {
            if (body) {
                setUser({ ...user, ...body })
                const cross = document.getElementById('modal-cross')
                Simulate.click(cross!)
                return true
            }
            return false
        }).catch(err => {
            console.log(err)
            return false
        })
    }

    return (
        <div className={styles.container}>
            <h1>Изменение имени</h1>
            <Input placeholder={'Имя'} type={'text'} value={firstName} setValue={setFirstName} className={styles.names_input} />
            <Input placeholder={'Фамилия'} type={'text'} value={lastName} setValue={setLastName} className={styles.names_input} />
            <Button type={'primary'} size={'M'} onClick={sendNewUserData} loading={isNameLoading} disabled={!firstName.length || !lastName.length}>Изменить</Button>
        </div>
    )
}

export default ChangeName;