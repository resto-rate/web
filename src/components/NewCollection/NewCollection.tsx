import React, {useState} from "react";

import styles from "./NewCollection.module.css";
import Input from "../UIKit/Input/Input";
import Button from "../UIKit/Button/Button";
import {createCollection} from "../../api/establishment";
import {useUserContext} from "../../context/userContext";
import {useCollectionsContext} from "../../context/CollectionsContext";

const NewCollection = ({onComplete}: {onComplete: () => void}) => {
    const {collections, setCollections} = useCollectionsContext();
    const {user} = useUserContext();
    const [collectionName, setCollectionName] = useState("");


    const create = async () => {
        await createCollection({Title: collectionName}, user.At.Token).then((resp) => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            const newCollections = structuredClone(collections)
            newCollections.Count += 1
            newCollections.Collections.push(body)
            setCollections(newCollections)
            onComplete()
        })
    }

    return (
        <div className={styles.container}>
            <h1>Новая коллекция</h1>
            <Input type={"text"} value={collectionName} setValue={setCollectionName} placeholder={"Название коллекции"}/>
            <Button type={"primary"} size={"M"} onClick={create}>Создать</Button>
        </div>
    )
}

export default NewCollection;
