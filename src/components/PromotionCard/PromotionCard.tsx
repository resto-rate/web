import React, {useState} from 'react';
import styles from './PromotionCard.module.css';
import Drawer from "../Drawer/Drawer";
import {numberToDate} from "../../utils/Functions";

export interface iRestPromotion {
    PromotionGUID: string,
    EstablishmentGUID: string,
    Title: string,
    Image: string,
    Conditions: string,
    StartTime: number,
    EndTime: number,
    ContactInfo: string,
    Undying: boolean
}

export interface iRestPromotionProps {
    promotion: iRestPromotion,
    classname?: any,
    onClick?: () => void
}

export const PromotionCard = (props: iRestPromotionProps) => {
    const [isDrawerOpen, setDrawerOpen] = useState(false);

    const toggleDrawer = () => {
        setDrawerOpen(!isDrawerOpen);
    };

    return (
        <>
            <div className={styles.container} onClick={() => props.onClick ? props.onClick() : toggleDrawer()}>
                <img src={props.promotion.Image ? props.promotion.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/promotion_placeholder.jpg'} style={{width: '100%'}}/>
            </div>
            <Drawer type={"promo"} photo={props.promotion.Image}
                    title={props.promotion.Title} description={props.promotion.Conditions}
                    startDate={numberToDate(props.promotion.StartTime)} endDate={numberToDate(props.promotion.EndTime)}
                    contacts={props.promotion.ContactInfo} isOpen={isDrawerOpen} onClose={toggleDrawer}/>
        </>
    )
}

export default PromotionCard;