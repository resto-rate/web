import React from "react";

import styles from './CollectionCard.module.css';
import { Link } from "react-router-dom";
import {switchEnding} from "../../utils/Functions";

interface iCollectionCardProps {
    CollectionGUID: string,
    Preview: string,
    Title: string,
    EstablishmentCount: number,
    AccountGUID: string
}

const CollectionCard: React.FC<iCollectionCardProps> = (props) => {
    return (
        <Link to={`/collection/${props.CollectionGUID}`} className={styles.card_collection}>
            <div className={styles.image_collection}>
                <img src={props.Preview} alt={props.Title}/>
            </div>
            <p className={styles.name_collection}>{props.Title}</p>
            <p className={styles.amount_collection}>{props.EstablishmentCount} {switchEnding(['мест', 'место', "мест"], props.EstablishmentCount)}</p>
        </Link>
    )
}

export default CollectionCard;