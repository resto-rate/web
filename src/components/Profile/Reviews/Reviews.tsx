import React, {useContext, useEffect, useState} from 'react'
import Review, {iReviews} from "../../Review/Review";
import EmptyStates from "../../EmptyStates/EmptyStates";
import {getUserReviews} from "../../../api/establishment";
import {UserContext} from "../../../context/userContext";
import {Col} from "react-bootstrap";
import Row from "react-bootstrap/Row";

export default function Reviews() {
    const {user} = useContext(UserContext);


    const [reviews, setReviews] = useState<iReviews>({Count: 0, Reviews: []});

    const getReviews = async () => {
        await getUserReviews(user.AccountGUID).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setReviews(body)
            } else {
                console.log('Wrong user id')
            }
        })
    }

    const onDelete = (ind: number) => {
        const newReviews = structuredClone(reviews)
        newReviews.Reviews.splice(ind, 1)
        newReviews.Count -= 1
        setReviews(newReviews)
    }
    useEffect(() => {
        getReviews()
    }, []);

    if (reviews === undefined) {
        return (<div>Загрузка...</div>)
    }

    return (
        <div>
            {reviews.Count === 0 || reviews.Reviews.length === 0 ?
                <EmptyStates type={"empty"} heading={'Пока пусто'} text={'Здесь будут оставденные Вами отзывы'}/>
                :
                <Row>
                    <Col md={2}/>
                <Col md={8}>
                    {
                        reviews.Reviews.map((r, ind) => <Review type={'user'} review={r} onDelete={() => onDelete(ind)} key={ind} />)
                    }
                </Col>
                    <Col md={2}/>
                </Row>
            }

        </div>
    )
}
