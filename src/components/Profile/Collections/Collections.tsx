import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import CollectionCard from '../../CollectionCard/CollectionCard'
import Button from '../../UIKit/Button/Button'
import Search from '../../Search/Search'

import styles from './Collections.module.css'
import Modal from "../../Modal/Modal";
import NewCollection from "../../NewCollection/NewCollection";
import EmptyStates from "../../EmptyStates/EmptyStates";
import {useUserContext} from "../../../context/userContext";
import {useCollectionsContext} from "../../../context/CollectionsContext";

export default function Collections() {
    const {collections} = useCollectionsContext();
    const [searchCollections, setSearchCollections] = useState('');
    const [openNewCollection, setOpenNewCollection] = useState(false);

    return (
        <>
            <Row className={styles.section}>
                <Col xs={2}></Col>
                <Col xs={6}>
                    {
                    <Search placeholder={'Поиск по подборкам'} color={"orange"} />
                    }
                </Col>
                <Col xs={2}>
                    <Button type={"primary"} size={"M"} onClick={() => {setOpenNewCollection(true)}}>Добавить подборку</Button>
                </Col>
                <Col xs={2}></Col>
            </Row>

            <Row className={styles.collections}>
            {
                collections.Count > 0 ?
                    collections.Collections.map((c, ind) => <Col md={3}><CollectionCard {...c} key={ind} /></Col>)
                    :
                    <EmptyStates type={"empty"} heading={'Пока пусто'} text={'Здесь будут Ваши коллекции заведений'} />
            }
            </Row>

            <Modal open={openNewCollection} setOpen={setOpenNewCollection}>
                <NewCollection onComplete={() => setOpenNewCollection(false)} />
            </Modal>
        </>
    )
}
