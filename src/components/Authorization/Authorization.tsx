import React, { useEffect, useState } from "react";
import { Simulate } from "react-dom/test-utils";
import QRCode from "react-qr-code";
import cn from "classnames";

import Input from "../UIKit/Input/Input";
import Button from "../UIKit/Button/Button";
import Icons from "../UIKit/Icons";
import Code from "../UIKit/Code/Code";
import { changeProfileData, getOtpCode, getProfile, loginOtp } from "../../api/auth";
import { useUserContext } from "../../context/userContext";

import styles from './Authorization.module.css'
import { OTP_BOT_LINK } from "../../utils/Constants";
import {capitalizeFirst} from "../../utils/Functions";
import { useAlertContext } from "../../context/alertContext";


const Authorization = () => {
    const { user, setUser } = useUserContext();
    const { setAlert } = useAlertContext();
    const [stage, setStage] = useState<'phone' | 'phoneLoading' | 'otp' | 'otpLoading' | 'name' | 'nameLoading'>('phone');
    const [phone, setPhone] = useState('');
    const [OTPcode, setOTPcode] = useState('');
    const [retrySendOTPSeconds, setRetrySendOTPSeconds] = useState(0);
    const [otpGUID, setOtpGUID] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const [qrOpened, setQROpened] = useState(false);

    const clearFields = () => {
        setStage('phone');
        setPhone('');
        setOTPcode('');
        setOtpGUID('');
        setFirstName('');
        setLastName('');
    }

    React.useEffect(() => {
        retrySendOTPSeconds > 0 && setTimeout(() => setRetrySendOTPSeconds(retrySendOTPSeconds - 1), 1000);
    }, [retrySendOTPSeconds]);


    const sendPhone = async () => {
        setStage('phoneLoading')
        setOTPcode('')
        console.log({ Login: phone.substring(2) })
        if (phone.length !== 12) {
            console.log('Неверно указан номер телефона')
            setStage('phone')
            return
        }
        await getOtpCode({ Login: phone.substring(2) }).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
            setStage('phone')
            switch (resp.status) {
                case 400:
                    //alert('слишком много попыток');
                    break;
                default:
                    alert(`Произошла неизвестная ошибка, код ${resp.status}`);
                    break;
            }
        }).then(body => {
            console.log(body);
            if (body) {
                setOtpGUID(body.OtpGUID);
                setRetrySendOTPSeconds(body.AuthExpiredSeconds)
                setStage('otp')
            }
        }).catch(resp => {
            console.log(resp)
            setStage('phone')
        })
    }

    const sendOtp = async (code: string) => {
        setStage('otpLoading')
        console.log({ Login: phone.substring(2), OTP: code, OtpGUID: otpGUID })
        await loginOtp({ Login: phone.substring(2), OTP: code, OtpGUID: otpGUID }).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
            switch (resp.status) {
                case 400:
                    console.log('Неверный OTP')
                    setAlert({icon: 'attention', message: 'Неправильный код подтверждения'})
                    setStage('otp')
                    setOTPcode('')
                    return;
                case 404:
                    console.log('OTP устарел')
                    setAlert({icon: 'attention', message: 'Код подтверждения устарел'})
                    setStage('otp')
                    setOTPcode('')
                    return;
                default:
                    alert(`Произошла неизвестная ошибка, код ${resp.status}`);
                    break;
            }
            setStage('otp')
        }).then(body => {
            if (body) {
                console.log(body);
                const tmpUser = { ...user, ...structuredClone(body) }
                console.log(tmpUser)
                if (body.AccountStatus === 1) {
                    setStage('name')
                    setUser(tmpUser)
                } else {
                    getProfile(body.At.Token).then(resp => {
                        console.log(resp)
                        return resp.json()
                    }).then(body => {
                        console.log(body)
                        console.log({ ...user, ...tmpUser, ...body, Empty: false })
                        setUser({ ...user, ...tmpUser, ...structuredClone(body), Empty: false })
                        console.log('User saved')
                        localStorage.setItem('user', JSON.stringify(tmpUser))
                        clearFields()
                        Simulate.click(document.getElementById('modal-cross')!)
                    })
                }
            }
        })
    }

    const sendNewUserData = async () => {
        setStage('nameLoading')
        console.log(firstName, lastName)
        if (!firstName || !lastName) {
            console.log('Пустые поля!')
            setStage('name')
            return
        }
        const city = localStorage.getItem('city')!
        await changeProfileData({ Name: capitalizeFirst(firstName), Surname: capitalizeFirst(lastName), City: city }, user.At.Token).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json();
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setUser({ ...user, ...structuredClone(body), Empty: false })
                localStorage.setItem('user', JSON.stringify({ ...user, ...structuredClone(body), Empty: false }))
                clearFields()
                Simulate.click(document.getElementById('modal-cross')!)
            }
        })
    }

    useEffect(() => {
        document.getElementById('modal-cross')!.addEventListener('click', () => {
            console.log('clear')
            clearFields()
        });
    }, []);

    switch (stage) {
        case 'phone':
        case 'phoneLoading':
            return (
                <div className={styles.container}>
                    <h1 className={styles.title}>Войти<br />или зарегистрироваться</h1>
                    <Input type={'phone'} value={phone} setValue={setPhone} className={styles.phone_input}
                        placeholder={'Введите ваш номер телефона'} required={true} />
                    <span className={styles.phone_comment}>На указанный номер телефона придет код подтверждения</span>
                    <div onClick={() => setQROpened(!qrOpened)} style={{ cursor: 'pointer' }}>
                        <div className={styles.tlg_open}>
                            <span className={styles.tlg_title}>Получать одноразовые пароли в Telegram</span>
                            <Icons icon={qrOpened ? 'chevron-up' : 'chevron-down'} className={styles.tlg_chevron} />
                        </div>
                        <div className={cn(styles.qr_container, qrOpened ? "" : styles.hidden)}>
                            <QRCode className={styles.qr} size={128} value={OTP_BOT_LINK ? OTP_BOT_LINK : 'No link provided'} />
                            <span>Отправьте боту свой контакт,<br />чтобы получать коды<br />подтверждения</span>
                        </div>
                    </div>
                    <Button type={'primary'} size={'M'} onClick={sendPhone} disabled={phone ? phone.length !== 12 : true}
                        loading={stage === 'phoneLoading'}>Продолжить</Button>
                </div>
            )
        case 'otp':
        case 'otpLoading':
            return (
                <div className={styles.container}>
                    <div className={styles.otp_back} onClick={() => setStage('phone')}>
                        <Icons icon={'back'} />
                        <p>Назад</p>
                    </div>
                    <h1 className={styles.title}>Введите код</h1>
                    <Code value={OTPcode} setValue={setOTPcode} sendData={sendOtp} className={styles.code_input}
                        retry={retrySendOTPSeconds} retryFunc={sendPhone} />
                </div>
            )
        case 'name':
        case 'nameLoading':
            return (
                <div className={styles.container}>
                    <h1 className={styles.title}>Давайте знакомиться!</h1>
                    <Input placeholder={'Имя'} type={'text'} value={firstName} setValue={setFirstName} className={styles.names_input} />
                    <Input placeholder={'Фамилия'} type={'text'} value={lastName} setValue={setLastName} className={styles.names_input} />
                    <Button type={'primary'} size={'M'} onClick={sendNewUserData} loading={stage === 'nameLoading'}>Сохранить</Button>
                </div>
            )
    }
}

export default Authorization
