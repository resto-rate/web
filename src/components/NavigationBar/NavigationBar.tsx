import React from "react";

import styles from "./NavigationBar.module.css";
import { NavLink, useParams } from "react-router-dom";
import { Row } from "react-bootstrap";

const NavigationBar = () => {
    const params = useParams();
    const establishmentId = params.id;

    return (
        <Row>
            <ul className={styles.menu}>
                <li><NavLink to={`/my-rests/${establishmentId}`}>Превью</NavLink></li>
                <li><NavLink to={`/my-rests/${establishmentId}/edit`}>Основная информация</NavLink></li>
                <li><NavLink to={`/my-rests/${establishmentId}/menu`}>Меню</NavLink></li>
                <li><NavLink to={`/my-rests/${establishmentId}/promotions-events`}>Акции и Мероприятия</NavLink></li>
            </ul>
        </Row>
    )
}

export default NavigationBar;