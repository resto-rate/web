import React, {useState} from "react";
import styles from './EventCard.module.css';
import {Col} from "react-bootstrap";
import {numberToDate} from "../../utils/Functions";
import Drawer from "../Drawer/Drawer";
import Chips from "../UIKit/Chips/Chips";

export interface iRestEvent {
    EventGUID: string,
    EstablishmentGUID: string,
    EstablishmentTitle: string,
    Title: string,
    Image: string,
    Description: string,
    DateTime: number,
    ContactInfo: string
}

export interface iRestEventProps {
    event: iRestEvent,
    classname?: any,
    onClick?: () => void
}

const EventCard = (props: iRestEventProps) => {
    const date = new Date(props.event.DateTime * 1000)
    const [isDrawerOpen, setDrawerOpen] = useState(false);

    const toggleDrawer = () => {
        setDrawerOpen(!isDrawerOpen);
    };
    return (
        <>
            <div className={styles.event} onClick={() => props.onClick ? props.onClick() : toggleDrawer()}>
                <Col>
                    <img
                        src={props.event.Image ? props.event.Image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/event_placeholder.jpg'}
                        className={styles.img}/>
                </Col>
                <Col className={styles.event_content}>
                    <div>
                        <Chips color={"dark"} label={props.event.EstablishmentTitle}/>
                    </div>
                    <p className={styles.date}>Дата: {date.toLocaleDateString()} в {date.toLocaleTimeString()}</p>
                    <p className={styles.title}>{props.event.Title}</p>
                    <p className={styles.description}>{props.event.Description}</p>
                    <p className={styles.contact}><span>Подробнее: </span>{props.event.ContactInfo}</p>
                </Col>
            </div>
            <Drawer type={"event"} photo={props.event.Image}
                    title={props.event.Title} description={props.event.Description}
                    startDate={date.toLocaleDateString()} time={date.toLocaleTimeString()}
                    contacts={props.event.ContactInfo} isOpen={isDrawerOpen} onClose={toggleDrawer} rest={props.event.EstablishmentTitle}/>
        </>
    )
}

export default EventCard;