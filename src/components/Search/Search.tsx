import React, {useEffect, useState} from "react";
import cn from 'classnames';

import styles from './Search.module.css';
import Icons from "../UIKit/Icons";
import {useNavigate} from "react-router-dom";

interface iSearchProps {
    onClick?: Function,
    placeholder: string,
    color: 'orange' | 'rose',
    size?: '4' | '6',
    onChangeFn?: Function;
}

const Search: React.FC<iSearchProps> = ({placeholder, color, onChangeFn}) => {
    const inputProps = {placeholder};
    const navigate = useNavigate();
    const [value, setValue] = useState("");

   const onSearch = (e: any) => {
       if (e.key === "Enter") {
           navigate(`/search-results?q=${encodeURI(e.target.value)}`)
       }
    }

    return (
        <div className={cn(styles.search, styles[color])}>
            <input value={value} onChange={e => setValue(e.target.value)} className={styles.input} {...inputProps} id={'search-input'} onKeyDown={onSearch}/>
            <Icons icon={"search"} className={styles.icon}/>
        </div>
    )
}

export default Search;