import React, { useState } from "react";

import styles from './Header.module.css';
import Search from "../Search/Search";
import { Link } from "react-router-dom";

import Col from 'react-bootstrap/Col';

import logo from './logo-header.svg';
import Authorization from "../Authorization/Authorization";
import Modal from "../Modal/Modal";
import { initialUserContextState, useUserContext } from "../../context/userContext";
import { logout } from "../../api/auth";
import Icons from "../UIKit/Icons";
import ChangeCity from "../ChangeCity/ChangeCity";


const Header: React.FC = () => {
    const [search, setSearch] = useState('')
    const [openAuth, setOpenAuth] = useState(false);
    const [openChangeCity, setOpenChangeCity] = useState(false);
    const { user, setUser } = useUserContext()
    const [profileDropdown, setProfileDropdown] = useState(false);

    const logoutUser = async () => {
        console.log(user)
        await logout(user.At.Token).then(resp => {
            const localCity  = localStorage.getItem('city')
            if (resp.ok && localCity !== null) {
                console.log(resp)
                setUser({...initialUserContextState.user, City: localCity})
                localStorage.removeItem('user')
                console.log('user has been removed')
            }
        });
    }

    return (
        <>
            <header>
                <div className={styles.first_level}>
                    <Link to={'/'}><img src={logo} alt={'РестоРейт'} /></Link>
                    <ul className={styles.right_links}>
                        <li>{user.RestAdmin ? <Link to={'/my-rests'}>Мои заведения</Link> : <Link to={'/for-owners'}>Владельцам заведений</Link>}</li>
                        {
                            <li><Link to={'/events'}>Мероприятия</Link></li>
                        }
                        {
                            user.Empty ?
                                <li onClick={() => setOpenAuth(true)} id={'header_login'}>Войти</li>
                                :
                                <li className={styles.user}
                                    onMouseEnter={() => setProfileDropdown(true)}
                                    onMouseLeave={() => setProfileDropdown(false)}>
                                    <Link to={'/profile'}>
                                        {
                                            user.ImageURL ?
                                                <img className={styles.user_photo} src={user.ImageURL} />
                                                :
                                                <div className={styles.user_photo}>{user.Name ? user.Name[0] + user.Surname[0] : 'А'}</div>
                                        }
                                        <span className={styles.user_name}>{user.Name ? user.Name + ' ' + user.Surname : 'Аккаунт'}</span>
                                    </Link>
                                </li>

                        }
                    </ul>
                    {profileDropdown &&
                        <ul className={styles.profile_dd}
                            onMouseLeave={() => setProfileDropdown(false)}
                            onMouseEnter={() => setProfileDropdown(true)}
                        >
                            <li><Icons icon={'logout'} className={styles.logout_icon} /><a onClick={() => logoutUser()}>Выйти из аккаунта</a></li>
                        </ul>
                    }
                </div>
                <div style={{
                    position: 'absolute',
                    height: '48px',
                    width: '100vw',
                    top: 0,
                    left: 0,
                    backgroundColor: 'var(--header-footer-accent)',
                    zIndex: 0,
                }}></div>
                <div className={styles.second_level}>
                    <Col md={10}>
                        <Search placeholder={'Поиск по заведениям'} color={'orange'}/>
                    </Col>
                        <div className={styles.location} onClick={() => {
                            setOpenChangeCity(true)
                        }}>
                            <Icons icon={"geo"} />
                            <p>{user.City}</p>
                        </div>
                </div>
            </header>
            <Modal open={openAuth} setOpen={setOpenAuth}>
                <Authorization/>
            </Modal>

            <Modal open={openChangeCity} setOpen={setOpenChangeCity}>
                <ChangeCity />
            </Modal>
        </>
    )
}

export default Header;
