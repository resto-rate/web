import React from 'react';

import styles from "./Drawer.module.css";
import Icons from "../UIKit/Icons";
import cn from "classnames";
import Chips from "../UIKit/Chips/Chips";

interface iDrawerProps {
    type: 'promo' | 'event',
    photo: string,
    title: string,
    description: string,
    startDate: string,
    endDate?: string,
    time?: string
    contacts: string,
    isOpen: boolean,
    onClose: () => void,
    rest?: string
}

const Drawer: React.FC<iDrawerProps> = (props) => {
    return (
        <>
            <div className={props.isOpen ? styles.overlay : ''} onClick={props.onClose}/>
            <div className={cn(styles.container, props.isOpen ? styles.open : "")}>
                <div className={styles.drawer}>
                    <div className={styles.header}>
                        <Icons icon={"back"} onClick={() => {props.onClose()}} className={styles.icon_back}/>
                        {props.type === 'promo' ? <h2>Акция заведения</h2> : <h2>Событие заведения</h2>}
                    </div>
                    <div className={styles.content}>
                        {props.type === 'promo' ?
                            <img className={cn(styles.img, styles.promo_img)}
                                 src={props.photo ? props.photo : 'https://restorate.hb.ru-msk.vkcs.cloud/static/promotion_placeholder.jpg'}/>
                            :
                            <img className={cn(styles.img, styles.event_img)}
                                 src={props.photo ? props.photo : 'https://restorate.hb.ru-msk.vkcs.cloud/static/event_placeholder.jpg'}/>
                            }
                        {props.type === 'event' && props.rest && <div><Chips label={props.rest} color={"dark"}/></div>}
                        {props.type === 'event' && <p className={styles.date_time}>Дата: {props.startDate} {props.time}</p>}

                        <h3 className={styles.title}>{props.title}</h3>
                        <p className={styles.description}>{props.description}</p>

                        <p className={styles.contacts}><span>Подробнее: </span>{props.contacts}</p>
                    </div>
                </div>
            </div>
        </>

    )
}

export default Drawer;