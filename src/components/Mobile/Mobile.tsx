import React from "react";

import styles from "./Mobile.module.css";
import download from "./img/download.svg";
import app from "../../pages/Application/images/mobile.png";
import {Link} from "react-router-dom";

const Mobile = () => {
    return (
        <div className={styles.container}>
            <h1>Любимые заведения еще ближе</h1>
            <p>Скачать для Android</p>
            <img src={app} alt={"Android"} className={styles.app}/>

            <div className={styles.download}>
                <Link to={'https://gitlab.com/resto-rate/app/-/releases'}><img src={download} alt={"RuStore"} className={styles.rustore} /></Link>
            </div>
        </div>
    )
}

export default Mobile;