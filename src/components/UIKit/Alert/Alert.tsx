import React from 'react';

import {initialAlertContextState, useAlertContext} from "../../../context/alertContext";

import Icons from "../Icons";

import styles from "./Alert.module.css";


const Alert: React.FC = () => {
    const {alert} = useAlertContext();

    if (alert === initialAlertContextState.alert) {
        return null;
    }

    return (
        <div className={styles.alert}>
            <Icons icon={alert.icon} className={styles.icon}/>
            <p>{alert.message}</p>
        </div>
    )
}

export default Alert;
