import React from "react";
import cn from 'classnames';
import PhoneInput from "react-phone-number-input";

import styles from './Input.module.css';


interface iInputProps {
    type: 'text' | 'textarea' | 'phone' | 'time' | 'date' | 'number';
    value: string;
    setValue: (value: string) => void;
    placeholder?: string;
    required?: boolean;
    disabled?: boolean;
    className?: string;
}


const Input: React.FC<iInputProps> = ({ type, value, setValue, placeholder, required, disabled, className }) => {
    const inputProps = { value, placeholder, required, disabled, onChange: (event: any) => {setValue(event.target.value);console.log(event.target.value) }};


    const getInput: () => JSX.Element = () => {
        switch (type) {
            case 'textarea':
                return <textarea className={cn(styles.input, styles.textarea)} {...inputProps} />
            case 'phone':
                //@ts-ignore
                return <PhoneInput international countryCallingCodeEditable={false} defaultCountry="RU" {...inputProps} className={styles.phone_input} onChange={setValue} />
            default:
                return <input type={type} className={cn(styles.input, styles.text)} {...inputProps} />
        }
    }

    return (
        <div className={cn(styles.container, className)}>
            {getInput()}
        </div>
    );

}

export default Input;
