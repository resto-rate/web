import React from "react";

import styles from "./Stars.module.css";
import Icons from "../Icons";

interface iStarsProps {
    rating: number,
    size: 's18' | 'm' | 'l'
}

const Stars: React.FC<iStarsProps> = ({rating, size}) => {
    return (
        <div>
            {[0, 0, 0, 0, 0].map((el, i) => {
                if (rating < i + 0.25) {
                    return <Icons key={i} icon={"star-empty"} className={styles[size]}/>
                } else if (i + 0.25 <= rating && rating < i + 0.75) {
                    return <Icons key={i} icon={"star-half"} className={styles[size]}/>
                } else {
                    return <Icons key={i} icon={"star"} className={styles[size]}/>
                }
            })}
        </div>
    )
}

export default Stars;