import React from 'react';
import styles from './StarsInput.module.css';
import Icons from "../Icons";
import cn from "classnames";

interface iStarsInputProps {
    criterion: string;
    value: number;
    setValue: (value: number) => void;
}

const StarsInput: React.FC<iStarsInputProps> = (props) => {
    return (
        <div className={styles.container}>
            <h3>{props.criterion}</h3>
            <div className={styles.stars}>
                {
                    [1, 2, 3, 4, 5].map(item =>
                        <Icons icon={"star"} key={item} className={cn(styles.star, styles[`star${item}`], item <= props.value ? styles.active : '')} onClick={() => props.setValue(item)} />
                    )
                }
            </div>
        </div>
    )
}

export default StarsInput;
