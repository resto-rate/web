import React from "react";
import cn from 'classnames';

import styles from './Chips.module.css';

interface iChipsProps {
    onClick?: Function,
    label: string,
    color: 'dark' | 'light'
    className?: any
}

const Chips: React.FC<iChipsProps> = ({label, color, className}) => {
    return (
        <div className={cn(styles.chips, styles[color], className)}>
            {label}
        </div>
    )
}

export default Chips;
