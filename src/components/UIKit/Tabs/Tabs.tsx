import React from "react";
import { NavLink } from "react-router-dom";
import cn from "classnames";

import styles from './Tabs.module.css';


export interface iTabsProps {
    tabs: {
        title: string,
        link: string
    }[]
}

const Tabs: React.FC<iTabsProps> = ({ tabs }) => {
    return (
        <ul className={styles.container}>
            {tabs.map((tab, index) => <li key={index} className={styles.tab}><NavLink to={tab.link}>{tab.title}</NavLink></li>)}
        </ul>
    )
}

export default Tabs;
