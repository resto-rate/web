import React from 'react';
import cn from 'classnames';

import Icons from '../Icons';

import styles from './Checkbox.module.css';


interface iCheckboxProps {
    children: React.ReactNode;
    isChecked: boolean;
    setChecked: (isChecked: boolean) => void;
    disabled?: boolean;
}

const Checkbox: React.FC<iCheckboxProps> = ({children, isChecked, setChecked, disabled = false}) =>
    <label className={cn(styles.checkbox, isChecked && styles.checked, disabled && styles.disabled)}>
        <Icons icon={isChecked ? 'checkbox-marked' : 'checkbox-blank'} className={styles.checkbox__icon}
               onClick={() => !disabled && setChecked(!isChecked)}/>
        <span className={'primary__text'}>{children}</span>
    </label>;

export default Checkbox;
