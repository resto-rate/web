import React, { useEffect } from "react";
import $ from "jquery";


import styles from './Code.module.css';
import Button from "../Button/Button";

const Code: React.FC<{
    value: string,
    setValue: Function,
    sendData: (value: string) => void,
    retry: number,
    retryFunc: () => void,
    className?: string
}> = ({ value, setValue, sendData, retry, retryFunc, className }) => {

    const onPaste = (event: React.ClipboardEvent<HTMLInputElement>) => {
        const data = event.clipboardData.getData('Text')
        if (data && data.length == 6 && /^\d+$/.test(data)) {
            setValue(data)
            sendData(data)
        }
    }

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Backspace') {
            setValue(value.substring(0, value.length - 1))
        } else if ('0123456789'.includes(event.key)) {
            value.length === 5 && sendData(value + event.key)
            value.length < 6 && setValue(value + event.key)
        }

    }

    useEffect(() => {
        if (value.length < 6) {
            $('#auth-inputs').children('input')[value.length].focus()
        }
    }, [value]);

    return (
        <div className={className}>
            <div className={styles.container} id={'auth-inputs'}>
                <input className={styles.input} type={"number"} value={value.charAt(0)} onKeyDown={onKeyDown} onPaste={onPaste} autoFocus={true} />
                <input className={styles.input} type={"number"} value={value.charAt(1)} onKeyDown={onKeyDown} onPaste={onPaste} />
                <input className={styles.input} type={"number"} value={value.charAt(2)} onKeyDown={onKeyDown} onPaste={onPaste} />
                <input className={styles.input} type={"number"} value={value.charAt(3)} onKeyDown={onKeyDown} onPaste={onPaste} />
                <input className={styles.input} type={"number"} value={value.charAt(4)} onKeyDown={onKeyDown} onPaste={onPaste} />
                <input className={styles.input} type={"number"} value={value.charAt(5)} onKeyDown={onKeyDown} onPaste={onPaste} />
            </div>
            {
                retry ?
                    <span className={styles.retry}>Устареет через {retry}с.</span>
                    :
                    <Button type={"text"} size={"S"} onClick={retryFunc}>Отправить повторно</Button>
            }
        </div>
    )
}

export default Code;
