import React, { useState } from 'react'
import cn from 'classnames';

import styles from './Select.module.css';
import Icons from '../Icons';


interface iSelectProps {
    value: string;
    setValue: (value: string) => void;
    datalist: string[];
    placeholder?: string;
    className?: string;
}

const Select: React.FC<iSelectProps> = ({ value, setValue, datalist, placeholder, className }) => {
    const [listShowed, setListShowed] = useState(false)

    return (
        <div className={cn(styles.container, className)}>
            <div className={styles.selected} onClick={() => setListShowed(!listShowed)} style={listShowed ? { borderBottomLeftRadius: 0, borderBottomRightRadius: 0, backgroundColor: 'var(--orange-50)' } : {}}>
                {
                    value ?
                        <div>{value}</div>
                        :
                        <div className={styles.placeholder}>{placeholder}</div>
                }
                <Icons icon={listShowed ? 'chevron-up' : 'chevron-down'} />
            </div>
            {
                listShowed ?
                    <div className={styles.datalist}>
                        {
                            datalist.sort().map((el, ind) =>
                                <div key={ind} className={styles.datalist_item} onClick={() => { setValue(el); setListShowed(false) }}>{el}</div>
                            )
                        }
                    </div>
                    :
                    <></>
            }

        </div>
    )
}

export default Select
