import React from "react";
import cn from 'classnames';

import styles from './Button.module.css';

interface iButtonProps {
    type: 'primary' | 'secondary' | 'text',
    children: any,
    size: 'L' | 'M' | 'S',
    onClick: () => void,
    disabled?: boolean,
    loading?: boolean,
    className?: string
}

const Button: React.FC<iButtonProps> = ({type, children, size, onClick, disabled, loading, className}) => {
    return (
        <button
            onClick={onClick}
            disabled={disabled}
            className={cn(styles.button, styles[type], styles[size], loading && styles.loading, disabled && styles.disabled)}
        >
            {children}
            {loading && <span className={styles.loader}></span>}
        </button>
    )
}

export default Button;
