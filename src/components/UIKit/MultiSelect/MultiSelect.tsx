import React, { useEffect, useState } from 'react';
import cn from 'classnames';

import styles from './MultiSelect.module.css';


interface iMultiSelectProps {
    items: string[];
    value: string[];
    setValue: (value: string[]) => void;
    restriction?: number;
}

const MultiSelect: React.FC<iMultiSelectProps> = ({ items, value, setValue, restriction }) => {

    const selected = (el: string) => value.indexOf(el) !== -1

    const switchSelectElement = (el: string) => {
        const newValue = structuredClone(value)
        if (value.indexOf(el) === -1) {

            if (!restriction || value.length < restriction) {
                newValue.push(el)
            }
        } else {
            newValue.splice(newValue.indexOf(el), 1)
        }
        setValue(newValue)
    }

    return (
        <div className={styles.container}>
            {
                items.map((el, ind) =>
                    <div key={ind} className={cn(styles.item, selected(el) ? styles.choosen : styles.default)} onClick={() => switchSelectElement(el)}>{el}</div>
                )
            }
        </div>
    )
}

export default MultiSelect;
