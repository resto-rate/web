import React from "react";

import styles from "./DeleteCollection.module.css";
import Button from "../UIKit/Button/Button";

interface iDeleteCollectionProps {
    name: string;
    id: string;
}

const DeleteCollection:React.FC<iDeleteCollectionProps> = ({name, id}) => {

    const sendDeleteCollection = async () => {
        console.log("sendDeleteCollectionName");
        return 0;
    }

    return (
        <div className={styles.container}>
            <h1>Удаление коллекции</h1>
            <p>Вы уверены, что хотите удалить коллекцию <span>«{name}»</span>?</p>
            <Button type="primary" size={"M"} onClick={sendDeleteCollection}>Удалить</Button>
        </div>
    )
}

export default DeleteCollection;