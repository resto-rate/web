import React from "react";
import logo from "./logo-footer.svg";
import {Link} from "react-router-dom";

import styles from "./Footer.module.css";


const Footer: React.FC = () => {
    return (
        <footer>
            <div className={styles.footer}>
                <div className={styles.logo}>
                    <Link to={'/'}><img src={logo} alt={'РестоРейт'}/></Link>
                    <p>Сервис для поиска гастромических мест</p>
                </div>
                <div className={styles.links}>
                    <Link to={'/application'}>Мобильное приложение</Link>
                    <Link to={'/for-owners'}>Владельцам заведений</Link>
                </div>
            </div>
            <div style={{
                position: 'absolute',
                height: '80px',
                width: '100vw',
                bottom: 0,
                left: 0,
                backgroundColor: 'var(--surface-3)',
                zIndex: 0,
            }}></div>
        </footer>
    )
}

export default Footer;