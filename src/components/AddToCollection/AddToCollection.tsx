import React, {useState} from "react";
import { Col, Row } from 'react-bootstrap';
import {Link, useLocation, useNavigate} from "react-router-dom";

import Icons from "../UIKit/Icons";
import Button from "../UIKit/Button/Button";

import styles from "./AddToCollection.module.css";
import {useCollectionsContext} from "../../context/CollectionsContext";
import Checkbox from "../UIKit/Checkbox/Checkbox";
import {addToCollection} from "../../api/establishment";
import {useUserContext} from "../../context/userContext";
import NewCollection from "../NewCollection/NewCollection";
import Modal from "../Modal/Modal";


const AddToCollection = ({EstablishmentCollections, EstablishmentGUID}: {EstablishmentCollections: string[], EstablishmentGUID: string}) => {
    const {user} = useUserContext();
    const {collections, setCollections} = useCollectionsContext()
    const navigate = useNavigate()
    const [openNewCollection, setOpenNewCollection] = useState(false);

    const [opened, setOpened] = React.useState(false);

    const pushToCollection = async (CollectionGUID: string) => {
        if (EstablishmentGUID.length === 0 || CollectionGUID.length === 0 || user.Empty) {
            return
        }
        await addToCollection({EstablishmentGUID: EstablishmentGUID}, CollectionGUID, user.At.Token).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            console.log(body)
            navigate(`/collection/${CollectionGUID}`)
        })
    }

    const isChecked = (CollectionGUID: string) => {
        let res = false
        if (EstablishmentCollections === null) {
            return  false
        }
        EstablishmentCollections.forEach((collection) => {
            if (CollectionGUID === collection) {
                res = true;
            }
        })
        return res;
    }

    return (
        <div className={styles.container}>
            <Icons icon={'favorite-blank'} className={styles.favorite} onClick={() => setOpened(!opened)}/>
            {
                opened ?
                    <div className={styles.collections}>
                        {
                            collections.Collections.map(c =>
                                <div className={styles.collection}>
                                    <Checkbox isChecked={isChecked(c.CollectionGUID)}
                                              setChecked={() => pushToCollection(c.CollectionGUID)}>
                                        {c.Title}
                                    </Checkbox>
                                </div>
                            )
                        }
                        <Button type='primary' size='M' onClick={()=> setOpenNewCollection(true)}>Новая коллекция</Button>
                    </div>
                    :
                    <></>
            }

            <Modal open={openNewCollection} setOpen={setOpenNewCollection}>
                <NewCollection onComplete={() => setOpenNewCollection(false)} />
            </Modal>
        </div>
    )
}

export default AddToCollection
