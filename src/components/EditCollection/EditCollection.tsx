import React, {useState} from "react";

import styles from "./EditCollection.module.css";

import Input from "../UIKit/Input/Input";
import Button from "../UIKit/Button/Button";

interface iEditCollectionProps {
    name: string;
    id: string;
}

const EditCollection: React.FC<iEditCollectionProps> = ({name, id}) => {
    const [collectionName, setCollectionName] = useState(name);

    const sendNewCollectionName = async () => {
        console.log("sendNewCollectionName");
        return 0;
    }

    return (
        <div className={styles.container}>
            <h1>Изменение коллекции</h1>
            <Input placeholder={"Название коллекции"} type={"text"} value={collectionName} setValue={setCollectionName} />
            <Button type={"primary"} size={"M"} onClick={sendNewCollectionName}>Изменить</Button>
        </div>
    )
}

export default EditCollection;