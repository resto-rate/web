import React from "react";

import styles from "./RatingCriterion.module.css";
import Stars from "../UIKit/Stars/Stars";

interface iRatingCriterionProps {
    rating: number,
    criterion: string
}

const RatingCriterion:React.FC<iRatingCriterionProps> = ({rating, criterion}) => {
    return (
        <div className={styles.item}>
            <Stars rating={rating} size={'s18'}/>
            <p className={styles.criterion}>{criterion}</p>
        </div>
    )
}

export default RatingCriterion;