import {createContext, useContext} from 'react';

export interface iUser {
    Name: string;
    Surname: string;
    City: string;
    ImageURL: string;
    AccountGUID: string;
    At: { Exp: number, Token: string };
    Rt: { Exp: number, Token: string };
    RestAdmin: boolean;
    LinkObjectGUID: string;
    Empty: boolean;
}

export type UserContext = {
    user: iUser,
    setUser: (user: iUser) => void;
}

export const initialUserContextState: UserContext = {
    user: {
        Name: '',
        Surname: '',
        City: '',
        ImageURL: '',
        AccountGUID: '',
        At: {Exp: 0, Token: ''},
        Rt: {Exp: 0, Token: ''},
        RestAdmin: false,
        LinkObjectGUID: '',
        Empty: true,
    },
    setUser: (user: iUser) => {
    }
};

export const UserContext = createContext<UserContext>(initialUserContextState);
export const useUserContext = () => useContext(UserContext);
