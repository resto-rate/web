import {createContext, useContext} from 'react';

export interface iAlert {
    icon: 'attention' | 'share',
    message: string
}

export type AlertContextType = {
    alert: iAlert,
    setAlert: (alert: iAlert) => void;
}

export const initialAlertContextState: AlertContextType = {
    alert: {
        icon: "attention",
        message: "",
    },
    setAlert: (alert: iAlert) => 0
}

export const AlertContext = createContext(initialAlertContextState);

export const useAlertContext = () => useContext(AlertContext);

