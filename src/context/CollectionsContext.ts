import { createContext, useContext } from "react"

export interface ICollection {
    CollectionGUID: string
    Preview: string
    Title: string
    EstablishmentCount: number
    AccountGUID: string
}

type CollectionsContext = {
    collections: { Count: number, Collections: ICollection[] }
    setCollections: (collections: { Count: number, Collections: ICollection[] }) => void
}

export const initialCollectionsContextState: CollectionsContext = {
    collections: {
        Count: 0,
        Collections: [],
    },
    setCollections: (collections: { Count: number, Collections: ICollection[] }) => 0
}

export const CollectionsContext = createContext<CollectionsContext>(initialCollectionsContextState)
export const useCollectionsContext = () => useContext(CollectionsContext)

