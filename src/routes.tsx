import MainPage from './pages/MainPage/MainPage'
import Profile from "./pages/Profile/Profile";
import App from "./App";
import { Navigate } from 'react-router-dom';
import Collections from './components/Profile/Collections/Collections';
import Reviews from './components/Profile/Reviews/Reviews';
import CollectionPage from "./pages/CollectionPage/CollectionPage";
import Rest from './pages/Rest/Rest';
import Application from './pages/Application/Application';
import ForOwners from "./pages/ForOwners/ForOwners";
import CreatingRest from "./pages/CreatingRest/CreatingRest";
import Page404 from "./pages/Page404/Page404";
import Menu from "./pages/Menu/Menu";
import UpdatingMenu from "./pages/UpdatingMenu/UpdatingMenu";
import MyRests from "./pages/MyRests/MyRests";
import SearchResults from "./pages/SearchResults/SearchResults";
import MyRest from './pages/MyRest/MyRest';
import UpdatingPromotionsEvents from "./pages/UpdatingPromotionsEvents/UpdatingPromotionsEvents";
import RestReviews from "./pages/RestReviews/RestReviews";
import Events from "./pages/Events/Events";

export const PATH_PREFIX = '';

const routesConfig = [
    {
        element: <App />,
        children: [
            {
                path: PATH_PREFIX + '/',
                element: <MainPage />,
            },
            {
                path: PATH_PREFIX + '/*',
                element: <Page404 />
            },
            {
                path: PATH_PREFIX + '/profile',
                element: <Profile />,
                children: [
                    {
                        path: PATH_PREFIX + '/profile/collections',
                        element: <Collections />,
                    },
                    {
                        path: PATH_PREFIX + '/profile/reviews',
                        element: <Reviews />,
                    },
                    {
                        path: PATH_PREFIX + '/profile',
                        element: <Navigate to={PATH_PREFIX + '/profile/collections'} replace />
                    }
                ]
            },
            {
                path: PATH_PREFIX + '/my-rests',
                element: <MyRests />,
            },
            {
                path: PATH_PREFIX + '/my-rests/:id',
                element: <MyRest />,
                children: [
                    {
                        path: PATH_PREFIX + '/my-rests/:id',
                        element: <Rest />,
                    },
                    {
                        path: PATH_PREFIX + '/my-rests/:id/edit',
                        element: <CreatingRest />,
                    },
                    {
                        path: PATH_PREFIX + '/my-rests/:id/menu',
                        element: <UpdatingMenu />,
                    },
                    {
                        path: PATH_PREFIX + '/my-rests/:id/promotions-events',
                        element: <UpdatingPromotionsEvents />,
                    }
                ]
            },
            {
                path: PATH_PREFIX + '/collection/:id',
                element: <CollectionPage />,
            },
            {
                path: PATH_PREFIX + '/rest/:id',
                element: <Rest />,
            },
            {
                path: PATH_PREFIX + '/rest/:id/menu',
                element: <Menu />,
            },
            {
                path: PATH_PREFIX + '/rest/:id/reviews',
                element: <RestReviews />,
            },
            {
                path: PATH_PREFIX + '/application',
                element: <Application />
            },
            {
                path: PATH_PREFIX + '/for-owners',
                element: <ForOwners />
            },
            {
                path: PATH_PREFIX + '/create-rest',
                element: <CreatingRest />
            },
            {
                path: PATH_PREFIX + '/search-results',
                element: <SearchResults />
            },
            {
                path: PATH_PREFIX + '/events',
                element: <Events />
            },
            {
                path: PATH_PREFIX + '/*',
                element: <Navigate to={PATH_PREFIX + '/'} replace />
            }
        ]
    }
]

export default routesConfig;
