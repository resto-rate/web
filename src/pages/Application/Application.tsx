import React from "react"

import styles from "./Application.module.css"
import big_logo from "./images/big logo.svg"
import qr_app from "./images/qr_app.svg"
import ill1 from "./images/illustration 1.svg"
import ill2 from "./images/illustartion 2.svg"
import ill3 from "./images/illustartion 3.svg"
import mobile from "./images/mobile.png"

const Application = () => {
    return (
        <div className={styles.content}>
            <div className={styles.description}>
                <div className={styles.logo_qr}>
                    <img src={big_logo} alt={"logo"}/>
                    <img src={qr_app} alt={"qr"}/>
                </div>

                <div className={styles.title}>
                    <h1>Приложение РестоРейт</h1>
                    <p>Любимые заведения еще ближе</p>
                </div>

                <div className={styles.features}>
                    <div className={styles.feature}>
                        <img src={ill1} alt={"illustartion"}/>
                        <p>Легко ищите места по пути</p>
                    </div>
                    <div className={styles.feature}>
                        <img src={ill2} alt={"illustartion"}/>
                        <p>Будьте в курсе гастрономических событий вашего города</p>
                    </div>
                    <div className={styles.feature}>
                        <img src={ill3} alt={"illustartion"}/>
                        <p>Коллекции с вашими фаворитами всегда у вас под рукой</p>
                    </div>
                </div>
            </div>

            <img src={mobile} alt={"mobile"} className={styles.mobile}/>

        </div>
    )
}

export default Application;
