import React from "react"

import styles from "./ForOwners.module.css"
import Button from "../../components/UIKit/Button/Button"

import bell from "./images/bell.svg"
import new_inf from "./images/new.svg"
import star from "./images/star.svg"
import profile from "./images/profile.svg"
import chief from "./images/chief.svg"
import cake from "./images/cake.svg"
import {useNavigate} from "react-router-dom";

const ForOwners = () => {
    const navigate = useNavigate();
    return (
        <div className={styles.content}>

            <h1>Добавьте Ваше заведение на РестоРейт</h1>

            <div className={styles.features}>
                <div className={styles.row}>
                    <div className={styles.feature}>
                        <img src={bell} alt={"bell"} className={styles.icon}/>
                        <h3>Контакт с Вашими посетителями</h3>
                        <p>Клиенты будут получать уведомления об изменениях в заведениях</p>
                    </div>

                    <div className={styles.feature}>
                        <img src={new_inf} alt={"new_inf"} className={styles.icon}/>
                        <h3>Актуальная информация о заведении</h3>
                        <p>Легко редактируйте меню, фотографии, акции и другие данные о месте</p>
                    </div>

                    <img src={chief} alt={"chief"} className={styles.illustration}/>
                </div>


                <div className={styles.row}>
                    <img src={cake} alt={"cake"} className={styles.illustration}/>

                    <div className={styles.feature}>
                        <img src={star} alt={"star"} className={styles.icon}/>
                        <h3>Улучшение клиентского опыта</h3>
                        <p>Читайте отзывы ваших посетителей, чтобы улучшить сервис</p>
                    </div>

                    <div className={styles.feature}>
                        <img src={profile} alt={"profile"} className={styles.icon}/>
                        <h3>Быстрый доступ для сотрудников</h3>
                        <p>С легкостью делегируйте работу с РестоРейт Вашим администраторам</p>
                    </div>
                </div>
            </div>

            <Button type={'primary'} size={"L"} onClick={()  => { navigate("/create-rest") }}>Добавить заведение</Button>

        </div>
    )
}

export default ForOwners;