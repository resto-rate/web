import React, { useEffect, useState } from "react";

import styles from "./MyRests.module.css";
import Button from "../../components/UIKit/Button/Button";
import { useNavigate } from "react-router-dom";
import { useUserContext } from "../../context/userContext";

import emptyRests from "./img/empty-rests.svg";

import { getMyEstablishments } from "../../api/establishment";
import RestCard, { iRestCardProps } from "../../components/RestCard/RestCard";

const MyRests = () => {
    const [myRests, setMyRests] = useState<{ Count: number, Establishments: iRestCardProps[] }>({ Count: -1, Establishments: [] });
    const navigate = useNavigate();
    const { user } = useUserContext();

    const fetchData = async () => {
        console.log(user)
        if (user.Empty) {
            return
        }
        await getMyEstablishments(user.At.Token, user.AccountGUID).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
                return null
            }
        }).then(body => {
            console.log(body)
            setMyRests(body)
        })
    }

    useEffect(() => {
        fetchData()
    }, [user])

    return (
        <div className={styles.container}>
            <h1>Мои заведения</h1>
            {
                myRests.Count === -1 ?
                    <div>Загрузка...</div>
                    :
                    myRests.Count === 0 ?
                        <img src={emptyRests} alt="emptyRests" className={styles.img} />
                        :
                        myRests.Establishments.map((rest, ind) =>
                            <div className={styles.rest} key={ind}>
                                <RestCard {...rest} />
                            </div>
                        )
            }
            <div className={styles.button_container}><Button type={"primary"} size={"L"} onClick={() => navigate("/create-rest")} className={styles.button}>Добавить заведение</Button></div>
        </div>
    )
}

export default MyRests;