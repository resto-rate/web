import React, { useEffect, useState } from "react";

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import styles from './MainPage.module.css';
import Chips from "../../components/UIKit/Chips/Chips";
import Button from "../../components/UIKit/Button/Button";
import iconApp from "./images/IconApp.svg";
import RestCardShort from "../../components/RestCardShort/RestCardShort";
import Cuisine from "./Cuisine/Cuisine";
import Search from "./Search/Search";
import Reason from "./Reason/Reason";
import app from "./images/app.png";
import download from "../../components/Mobile/img/download.svg";
import { Link, useNavigate } from "react-router-dom";
import { useUserContext } from "../../context/userContext";
import { iRestCardProps } from "../../components/RestCard/RestCard";
import { searchEstablishments } from "../../api/establishment";
import { openAuth } from "../../utils/Functions";

const popularRequests = ['Коктейли', 'Кофейня', 'Стейки', 'Кондитерская', 'Хачапури', 'Пицца']

const reasons = [
    {
        title: 'Романтический вечер',
        description: 'Места с особой атмосферой и предложениями для влюбленных',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Romantic.svg',
        s: 'Романтический вечер'
    },
    {
        title: 'Банкеты и мероприятия',
        description: 'Заведения для корпоративов, юбилеев и свадеб, которые вместят всех ваших гостей',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Party.svg',
        s: 'Банкеты'
    },
    {
        title: 'Встречи с друзьями',
        description: 'Заведения для корпоративов, юбилеев и свадеб, которые вместят всех ваших гостей',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Friends.svg',
        s: 'Для большой компании'
    },
    {
        title: 'Обед с семьей',
        description: 'Места с детским меню и развлечениями',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Family.svg',
        s: 'Семейный обед'
    },
]

const cuisines = [
    {
        title: 'Вкус Италии',
        numberPlaces: 45,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Italy.jpg',
        query: 'итальянская'
    },
    {
        title: 'Вкус Грузии',
        numberPlaces: 12,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Georgia.jpg',
        query: 'грузинская'
    },
    {
        title: 'Вкус Азии',
        numberPlaces: 34,
        photo: 'https://restorate.hb.ru-msk.vkcs.cloud/static/Asia.jpg',
        query: 'японская'
    },
]

const MainPage: React.FC = () => {
    const { user } = useUserContext();
    const navigate = useNavigate();

    const [bestRests, setBestRests] = useState<iRestCardProps[] | null>(null)

    const fetchData = async () => {
        await searchEstablishments(0, {city: user.City}).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
                return null
            }
        }).then(body => {
            console.log(body)
            setBestRests(body.Establishments)
        })
    }

    useEffect(() => {
        if (user.City !== '') {
            fetchData()
        }
    }, [user])

    return (
        <>
            <div className={styles.carousel}></div>
            <div className={styles.title_description}>
                <h1 className={styles.heading1}>Найдем заведение на ваш вкус!</h1>
                <p className={styles.description}>Десятки заведений с честными отзывами в 40 городах России</p>
            </div>
            <Row>
                <Col md={2}></Col>
                <Col md={8}><Search /></Col>
                <Col md={2}></Col>
            </Row>
            <div className={styles.popular_requests}>
                <p>Популярные запросы:</p>
                <div className={styles.requests}>
                    {popularRequests.map((item, ind) =>
                        <Link key={ind} to={`/search-results?q=${item}`}><Chips label={item} color={"light"} /></Link>
                    )}
                </div>
            </div>
            <div className={styles.content}>
                <Row>
                    <Col md={6}>
                        <div className={styles.feature}>
                            <h3>Составьте личный гид по местам</h3>
                            {user.Empty ? <p>Авторизируйтесь, чтобы создавать персональные коллекции и не потерять любимые заведения</p> : <p>Сохраняйте заведения в персональные подборки в профиле</p>}
                            <img src={'https://restorate.hb.ru-msk.vkcs.cloud/static/guide.svg'} alt={'Гид'}
                                className={styles.guidePhoto} />
                            {user.Empty ?
                                <Button className={styles.go2Collections} type={"primary"} onClick={openAuth} size={"L"}>Войти</Button>
                                : <Button className={styles.go2Collections} type={"primary"} onClick={() => { navigate("/profile/collections") }} size={"L"}>Перейти к коллекциям</Button>}
                        </div>
                    </Col>
                    <Col md={6}>
                        <div className={styles.feature}>
                            <h3>Почувствуйте себя ресторанным критиком</h3>
                            <p>Оценивайте заведения и помогайте другим пользователям сделать правильный выбор</p>
                            <img src={'https://restorate.hb.ru-msk.vkcs.cloud/static/critic.svg'} alt={'Критик'}
                                className={styles.criticPhoto} />
                        </div>
                    </Col>
                </Row>
                <div className={styles.best}>
                    <div className={styles.best_all}>
                        <h2>Лучшее в городе</h2>
                        <Button type={"text"} size={"L"} onClick={() => navigate('/search-results?q=')}>Посмотреть все</Button>
                    </div>
                    <Row>
                        {
                            bestRests?.slice(0, 4).map((el, ind) => <Col md={3} key={ind}><RestCardShort {...el} /></Col>)
                        }
                    </Row>
                </div>
                <div className={styles.popular}>
                    <h2>Популярные кухни</h2>
                    <Row className={styles.cuisines}>
                        {
                            cuisines.map((element, index) =>
                                <Col md={4} key={index} onClick={() => navigate(`/search-results?q=${element.query}`)}><Cuisine photo={element.photo} title={element.title}
                                    numberPlaces={element.numberPlaces} /></Col>)
                        }
                    </Row>
                </div>
                <div className={styles.mobile} onClick={() => navigate('/application')}>
                    <div className={styles.icon_title}>
                        <img src={iconApp} alt={'Иконка'} />
                        <p>Любимые заведения еще ближе!</p>
                    </div>
                    <img src={app} alt={'Приложение'} className={styles.app} />
                    <Link to={'https://gitlab.com/resto-rate/app/-/releases'} target={'_blank'} className={styles.rustore}><img src={download} alt={'RuStore'} /></Link>
                </div>
                <div className={styles.reasons}>
                    <h2>Места на любой случай</h2>
                    <Row className={styles.row_reasons}>
                        {
                            reasons.map((element, index) =>
                                <Col md={6} key={index} onClick={() => navigate(`/search-results?q=&s=${element.s}`)}><Reason photo={element.photo} title={element.title}
                                    description={element.description}
                                    numberPlaces={element.numberPlaces} /></Col>)
                        }
                    </Row>
                </div>
                <div className={styles.for_owners}>
                    <div className={styles.for_owners_info}>
                        <h2>Разместите свое заведение на РестоРейт</h2>
                        <p>Следите за мнением посетителей, размещайте актуальную информацию и продвигайте бизнес</p>
                        <Button type={"primary"} size={"L"} onClick={() => { navigate("/for-owners") }}>Подробнее</Button>
                    </div>
                    <img src={'https://restorate.hb.ru-msk.vkcs.cloud/static/food.svg'} alt={''}></img>
                </div>
            </div>
        </>
    )
}

export default MainPage;
