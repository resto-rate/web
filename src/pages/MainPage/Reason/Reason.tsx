import React from "react";

import styles from "./Reason.module.css";
import {switchEnding} from "../../../utils/Functions";


interface iReasonProps {
    title: string,
    description: string,
    numberPlaces: number,
    photo: string
}
const Reason:React.FC<iReasonProps> = (props) => {
    return (
        <div className={styles.item}>
            <h5>{props.title}</h5>
            <p>{props.description}</p>

            <div className={styles.number_places}>{props.numberPlaces} {switchEnding(['мест', 'место', "мест"], props.numberPlaces)}</div>
            <img src={props.photo} alt={'Фото'} className={styles.image}/>
        </div>
    )
}

export default Reason;