import React, { useEffect, useState } from 'react';
import cn from 'classnames';

import { getInputVars } from '../../../api/establishment';
import { inputVarsType } from '../../CreatingRest/CreatingRest';

import Button from '../../../components/UIKit/Button/Button';
import Icons from "../../../components/UIKit/Icons";

import styles from './Search.module.css';


const SearchInput = (props: { datalist: string[], label: string, placeholder: string, value: string, setValue: (value: string) => void }) => {
    const [listShowed, setListShowed] = useState(false)

    return (
        <div className={styles.input_container} onClick={() => setListShowed(!listShowed)}>
            <div className={cn(styles.input_label, listShowed ? styles.active : '')}>{props.label}</div>
            {
                props.value ?
                    <div className={styles.input_value}>{props.value}</div>
                    :
                    <div className={styles.input_placeholder}>{props.placeholder}</div>
            }
            {
                listShowed ?
                    <>
                        <div className={styles.backdrop} />
                        <div className={styles.datalist}>
                            {
                                props.datalist.sort().map((el, ind) =>
                                    <div key={ind} className={styles.datalist_item} onClick={() => { props.setValue(el); setListShowed(false) }}>{el}</div>
                                )
                            }
                        </div>
                    </>
                    :
                    <></>
            }
        </div>
    )
}

const Search = () => {
    const [searchVars, setSearchVars] = useState<inputVarsType | null>(null)
    const [cuisine, setCuisine] = useState('');
    const [cuisines, setCuisines] = useState<string[]>([]);
    const [type, setType] = useState('');
    const [types, setTypes] = useState<string[]>([]);
    const [reason, setReason] = useState('');
    const [reasons, setReasons] = useState<string[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            await getInputVars().then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then(body => {
                if (body) {
                    console.log(body)
                    setSearchVars(body)
                }
            })
        }
        fetchData()
    }, [])

    useEffect(() => {
        if (searchVars === null) {
            return
        }
        setCuisines(searchVars.CuisineTypes.sort())
        setTypes(searchVars.EstablishmentTypes.sort())
        setReasons(searchVars.SuitableCases.sort())
    }, [searchVars])


    return (
        <div className={styles.container}>
            <div className={styles.inputs}>
                <SearchInput datalist={cuisines} label={'Кухня'} placeholder={'Что хотите поесть?'} value={cuisine} setValue={setCuisine} />
                <SearchInput datalist={types} label={'Формат заведения'} placeholder={'Куда пойти?'} value={type} setValue={setType} />
                <SearchInput datalist={reasons} label={'Случай'} placeholder={'Какой повод?'} value={reason} setValue={setReason} />
            </div>
            <Button type={'primary'} size={'L'} onClick={() => 0} ><Icons icon={'search'} className={styles.icon_search} /></Button>
        </div>

    )
}

export default Search;
