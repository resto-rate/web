import React from "react";

import styles from './Cuisine.module.css'
import {switchEnding} from "../../../utils/Functions";

interface iCuisineProps {
    photo: string,
    title: string,
    numberPlaces: number
}

const Cuisine:React.FC<iCuisineProps> = ({photo, title, numberPlaces}) => {
    return (
        <div className={styles.item} style={{backgroundImage: `url("${photo}")`}}>
            <h5 className={styles.title}>{title}</h5>
            <div className={styles.number_places}>{numberPlaces} {switchEnding(['мест', 'место', "мест"], numberPlaces)}</div>
        </div>
        )
}

export default Cuisine;
