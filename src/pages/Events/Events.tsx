import React, {useEffect, useState} from "react";
import styles from "./Events.module.css";
import EventCard, {iRestEvent, iRestEventProps} from "../../components/EventCard/EventCard";
import Row from "react-bootstrap/Row";
import {Col} from "react-bootstrap";
import {getEvents} from "../../api/establishment";
import {useUserContext} from "../../context/userContext";

const Events = () => {
    const {user} = useUserContext();
    const [events, setEvents] = useState<{ Count: number, Events: iRestEvent[] }>({Count: 0, Events: []});

    const fetchEvents = async () => {
        await getEvents(user.City).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setEvents({...body})
            } else {
                console.log('Wrong events id')
            }
        })
    }

    useEffect(() => {
        fetchEvents()
    }, []);

    return (
        <>
            <h1 className={styles.title}>Гастрономические события города</h1>
            <Row>
                {events.Events.map((e, ind) => <Col md={6}>
                    <EventCard key={ind} event={e}/>
                </Col>)}
            </Row>
        </>
    )
}

export default Events;