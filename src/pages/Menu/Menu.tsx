import React, {useEffect, useState} from "react";

import styles from "./Menu.module.css";
import MealCard, {iMeal} from "../../components/Rest/MealCard/MealCard";
import RestHeader from "../../components/Rest/RestHeader/RestHeader";
import Row from "react-bootstrap/Row";
import {Col} from "react-bootstrap";
import Search from "../../components/Search/Search";
import {getMenu} from "../../api/establishment";
import {useParams, useSearchParams} from "react-router-dom";

export interface iMenu {
    Count: number,
    DishesByCategory: {
        Category: string,
        Dishes: iMeal[]
    }[]
}

const Menu = () => {
    const [search, setSearch] = useState("");
    const [menu, setMenu] = useState<iMenu>();
    const [searchParams] = useSearchParams();
    const params = useParams();

    const establishmentId = params.id ? params.id : searchParams.get('id');

    const fetchMenu = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getMenu(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setMenu({...body})
            } else {
                console.log('Wrong establishment id')
            }
            console.log(menu)
        })
    }

    useEffect(() => {
        fetchMenu()
    }, [])

    if (menu === undefined) {
        return (<div>Загрузка...</div>)
    }

    return (
        <div className={styles.container}>
            <RestHeader/>
            <Row className={styles.title_search}>
                <Col md={8}>
                    <h1 className={styles.title}>Меню</h1>
                </Col>
                <Col md={4}>
                    {
                        //<Search placeholder={"Поиск по меню"} color={'orange'} value={search} setValue={setSearch}/>
                    }
                </Col>
            </Row>

            {
                menu.DishesByCategory.map((cat, indCat) =>
                    <>
                    <Row key={indCat}>
                        <h2 className={styles.category}>{cat.Category}</h2>
                    </Row>
                    <Row>
                        {
                            cat.Dishes.map((dish, indDish) =>
                                <Col md={3}>
                                    <MealCard meal={dish}/>
                                </Col>
                            )
                        }
                    </Row>
                    </>)

                    }

                    </div>
                )
            }

            export default Menu;
