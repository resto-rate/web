import React, { useEffect, useState } from "react";

import styles from "./CreatingRest.module.css";
import Row from "react-bootstrap/Row";
import { Col } from "react-bootstrap";
import Button from "../../components/UIKit/Button/Button";
import Input from "../../components/UIKit/Input/Input";

import vkIcon from "../../imgs/uil_vk.svg";
import tgIcon from "../../imgs/ic_baseline-telegram.svg";
import Checkbox from "../../components/UIKit/Checkbox/Checkbox";
import MultiSelect from "../../components/UIKit/MultiSelect/MultiSelect";
import Select from "../../components/UIKit/Select/Select";
import { RestType, createUpdateRest, getInputVars, searchAddress, getEstablishment, uploadImage } from "../../api/establishment";
import { withOfferToSignIn } from "../../hoc/withOfferToSignIn";
import { numberToTime, timeToNumber } from "../../utils/Functions";
import { iRest } from "../Rest/Rest";
import { useNavigate, useParams } from "react-router-dom";
import { useUserContext } from "../../context/userContext";
import { useAlertContext } from "../../context/alertContext";
import Icons from "../../components/UIKit/Icons";

export type inputVarsType = {
    EstablishmentTypes: string[]
    DishCategories: string[]
    AdditionalServices: string[]
    CuisineTypes: string[]
    SuitableCases: string[]
}


const CreatingRest = () => {
    const navigate = useNavigate()
    const { user } = useUserContext()
    const { setAlert } = useAlertContext()

    const params = useParams();
    const establishmentId = params.id;

    const [establishmentTitle, setEstablishmentTitle] = useState("")
    const [format, setFormat] = useState("");
    const [cuisines, setCuisines] = useState<string[]>([])
    let uploadingThing: 'LOGO' | 'PHOTOS' = 'LOGO';
    const [logo, setLogo] = useState<string>('')
    const [photos, setPhotos] = useState<string[]>([])

    const [address, setAddress] = useState("");
    const [phone, setPhone] = useState(""); // массив телефонов
    const [site, setSite] = useState("");
    const [telegram, setTelegram] = useState("");
    const [vk, setVk] = useState("")

    const [MondayStart, setMondayStart] = useState("00:00");
    const [MondayEnd, setMondayEnd] = useState("00:00");
    const [MondayOff, setMondayOff] = useState(false);
    const [TuesdayStart, setTuesdayStart] = useState("00:00");
    const [TuesdayEnd, setTuesdayEnd] = useState("00:00");
    const [TuesdayOff, setTuesdayOff] = useState(false);
    const [WednesdayStart, setWednesdayStart] = useState("00:00");
    const [WednesdayEnd, setWednesdayEnd] = useState("00:00");
    const [WednesdayOff, setWednesdayOff] = useState(false);
    const [ThursdayStart, setThursdayStart] = useState("00:00");
    const [ThursdayEnd, setThursdayEnd] = useState("00:00");
    const [ThursdayOff, setThursdayOff] = useState(false);
    const [FridayStart, setFridayStart] = useState("00:00");
    const [FridayEnd, setFridayEnd] = useState("00:00");
    const [FridayOff, setFridayOff] = useState(false);
    const [SaturdayStart, setSaturdayStart] = useState("00:00");
    const [SaturdayEnd, setSaturdayEnd] = useState("00:00");
    const [SaturdayOff, setSaturdayOff] = useState(false);
    const [SundayStart, setSundayStart] = useState("00:00");
    const [SundayEnd, setSundayEnd] = useState("00:00");
    const [SundayOff, setSundayOff] = useState(false);

    const [description, setDescription] = useState("");
    const [avgBill, setAvgBill] = useState("")
    const [reasons, setReasons] = useState<string[]>([]);
    const [services, setServices] = useState<string[]>([]);

    const [suggestedAddresses, setSuggestedAddresses] = useState([]);
    const [addressSelected, setAddressSelected] = useState(false);

    const [inputVars, setInputVars] = useState<inputVarsType>({ EstablishmentTypes: [], DishCategories: [], AdditionalServices: [], CuisineTypes: [], SuitableCases: [] });

    const fetchVars = async () => {
        await getInputVars().then(resp => {
            if (resp.ok) {
                return resp.json()
            }
            return null
        }).then(body => {
            console.log(body)
            if (body) {
                setInputVars(body)
            }
        })
    }

    const fetchEstablishment = async () => {
        if (establishmentId === undefined) {
            return
        }
        await getEstablishment(establishmentId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then((body: iRest) => {
            console.log(body)
            setEstablishmentTitle(body.EstablishmentTitle)
            setFormat(body.Type)
            setCuisines(body.Cuisines)
            setLogo(body.EstablishmentLogo)
            setPhotos(body.Images)

            setAddress(body.Address)
            setAddressSelected(true)
            setPhone('+7' + body.Contacts.MobilePhone)
            setTelegram(body.Contacts.Telegram)
            setVk(body.Contacts.VK)

            setMondayStart(numberToTime(body.Schedule.Monday.OpenTime))
            setMondayEnd(numberToTime(body.Schedule.Monday.CloseTime))
            setMondayOff(body.Schedule.Monday.RestDay)

            setTuesdayStart(numberToTime(body.Schedule.Tuesday.OpenTime))
            setTuesdayEnd(numberToTime(body.Schedule.Tuesday.CloseTime))
            setTuesdayOff(body.Schedule.Tuesday.RestDay)

            setWednesdayStart(numberToTime(body.Schedule.Wednesday.OpenTime))
            setWednesdayEnd(numberToTime(body.Schedule.Wednesday.CloseTime))
            setWednesdayOff(body.Schedule.Wednesday.RestDay)

            setThursdayStart(numberToTime(body.Schedule.Thursday.OpenTime))
            setThursdayEnd(numberToTime(body.Schedule.Thursday.CloseTime))
            setThursdayOff(body.Schedule.Thursday.RestDay)

            setFridayStart(numberToTime(body.Schedule.Friday.OpenTime))
            setFridayEnd(numberToTime(body.Schedule.Friday.CloseTime))
            setFridayOff(body.Schedule.Friday.RestDay)

            setSaturdayStart(numberToTime(body.Schedule.Saturday.OpenTime))
            setSaturdayEnd(numberToTime(body.Schedule.Saturday.CloseTime))
            setSaturdayOff(body.Schedule.Saturday.RestDay)

            setSundayStart(numberToTime(body.Schedule.Sunday.OpenTime))
            setSundayEnd(numberToTime(body.Schedule.Sunday.CloseTime))
            setSundayOff(body.Schedule.Sunday.RestDay)

            setDescription(body.Description)
            setAvgBill(body.AvgBill.toString())
            setReasons(body.SuitableCases)
            setServices(body.AdditionalServices)
        })
    }

    useEffect(() => {
        fetchVars()
        if (establishmentId !== undefined) {
            fetchEstablishment()
        }
    }, []);

    useEffect(() => {
        const fetchAddresses = async (search: string) => {
            await searchAddress(search).then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
                return null
            }).then(body => {
                console.log(body)
                if (body) {
                    setSuggestedAddresses(body.Address)
                }
            })
        }
        if (!addressSelected) {
            fetchAddresses(address)
        } else {
            setSuggestedAddresses([])
            setAddressSelected(false)
        }
    }, [address]);

    const selectAddress = (address: string) => {
        setAddress(address);
        setAddressSelected(true);
    }


    const createUpdate = async () => {
        //TODO: Requirement fields check

        const body: RestType = {
            EstablishmentTitle: establishmentTitle,
            EstablishmentLogo: logo,
            Address: address,
            AvgBill: Number(avgBill),
            Type: format,
            Cuisines: cuisines,
            Description: description,
            Images: photos,
            Contacts: {
                MobilePhone: phone.substring(2),
                SiteURL: site,
                Telegram: telegram,
                VK: vk
            },
            AdditionalServices: services,
            SuitableCases: reasons,
            Schedule: {
                Monday: {
                    OpenTime: timeToNumber(MondayStart),
                    CloseTime: timeToNumber(MondayEnd),
                    RestDay: MondayOff,
                },
                Tuesday: {
                    OpenTime: timeToNumber(TuesdayStart),
                    CloseTime: timeToNumber(TuesdayEnd),
                    RestDay: TuesdayOff,
                },
                Wednesday: {
                    OpenTime: timeToNumber(WednesdayStart),
                    CloseTime: timeToNumber(WednesdayEnd),
                    RestDay: WednesdayOff,
                },
                Thursday: {
                    OpenTime: timeToNumber(ThursdayStart),
                    CloseTime: timeToNumber(ThursdayEnd),
                    RestDay: ThursdayOff,
                },
                Friday: {
                    OpenTime: timeToNumber(FridayStart),
                    CloseTime: timeToNumber(FridayEnd),
                    RestDay: FridayOff,
                },
                Saturday: {
                    OpenTime: timeToNumber(SaturdayStart),
                    CloseTime: timeToNumber(SaturdayEnd),
                    RestDay: SaturdayOff,
                },
                Sunday: {
                    OpenTime: timeToNumber(SundayStart),
                    CloseTime: timeToNumber(SundayEnd),
                    RestDay: SundayOff,
                },
            }
        }
        if (establishmentId !== undefined) {
            body.EstablishmentGUID = establishmentId
        }

        console.log(user)
        await createUpdateRest(body, user.At.Token).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
            return null
        }).then((body: iRest) => {
            if (body) {
                navigate(`/my-rests/${body.EstablishmentGUID}${establishmentId === undefined ? '/menu' : ''}`)
            }
            console.log(body)
        })
    }

    const update = async () => {

    }

    const upload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            console.log("Uploading file...");

            const formData = new FormData();
            // @ts-ignore
            formData.append("imageFile", e.target.files[0]);
            let result = null
            try {
                result = await uploadImage(formData, user.At.Token)
            } catch (error) {
                console.log(error)
                setAlert({
                    icon: "attention",
                    message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                });
            }
            if (result === null)
                return
            if (result.ok) {
                const data = await result.json();
                switch (uploadingThing) {
                    case 'LOGO':
                        setLogo(data)
                        console.log(logo)
                        break
                    case 'PHOTOS':
                        setPhotos(photos.concat([data]))
                        console.log(photos)
                        break
                }
                //@ts-ignore
                document.getElementById('photo_input').value = ''
            } else {
                console.log(result);
                switch (result.status) {
                    case 413:
                        setAlert({
                            icon: "attention",
                            message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                        });
                }
            }
        }
    }

    const uploadLogo = async () => {
        uploadingThing = 'LOGO'
        document.getElementById('photo_input')?.click()
    }

    const uploadPhotos = async () => {
        uploadingThing = 'PHOTOS'
        document.getElementById('photo_input')?.click()
    }

    const [draggingItem, setDraggingItem] = useState(null)

    const handleDragStart = (e: any, item: any) => {
        console.log('handleDragStart')
        setDraggingItem(item)
        e.dataTransfer.setData('text/plain', '');
    };

    const handleDragEnd = () => {
        console.log('handleDragEnd')
        setDraggingItem(null)
    };

    const handleDragOver = (e: any) => {
        console.log('handleDragOver')
        e.preventDefault();
    };

    const handleDrop = (e: any, targetItem: any) => {
        console.log('handleDrop')
        if (!draggingItem) return;

        const currentIndex = photos.indexOf(draggingItem);
        const targetIndex = photos.indexOf(targetItem);

        if (currentIndex !== -1 && targetIndex !== -1) {
            const newPhotos = structuredClone(photos)
            newPhotos.splice(currentIndex, 1);
            newPhotos.splice(targetIndex, 0, draggingItem);
            setPhotos(newPhotos);
        }
    };

    const removePhoto = (ind: number) => {
        const newPhotos = structuredClone(photos)
        newPhotos.splice(ind, 1)
        setPhotos(newPhotos)
    };

    return (
        <div>
            <h1 className={styles.title}>{establishmentId === undefined ? 'Создание' : 'Редактирование'} заведения</h1>
            <input type="file" id="photo_input" onChange={upload} hidden />

            <div className={styles.form}>
                <div className={styles.block}>
                    <h2 className={styles.subtitle}>Основное</h2>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Название</p>
                        </Col>
                        <Col md={4}>
                            <Input placeholder={"Название"} type={"text"} value={establishmentTitle}
                                setValue={setEstablishmentTitle} />
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Формат</p>
                        </Col>
                        <Col md={4}>
                            <Select placeholder={"Формат"} value={format} setValue={setFormat} datalist={inputVars.EstablishmentTypes} />
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Тип кухни</p>
                            <p className={styles.text_secondary}>до 5 категорий</p>
                        </Col>
                        <Col md={9}>
                            {inputVars.CuisineTypes.length > 0 ? <MultiSelect items={inputVars.CuisineTypes.sort()} value={cuisines} setValue={setCuisines} restriction={5} /> : <></>}
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Логотип</p>
                        </Col>
                        <Col md={9}>
                            <div className={styles.photos_container}>
                                {
                                    logo ?
                                        <img src={logo} style={{ width: 128, height: 128, borderRadius: 64, objectFit: 'cover' }} />
                                        :
                                        <></>
                                }
                                <Button type={'secondary'} size={'M'} onClick={uploadLogo}><Icons icon={logo.length === 0 ? 'add-photo' : 'edit'} className={styles.photo_icon} /></Button>
                            </div>
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Фотографии</p>
                            <p className={styles.text_secondary}>до 20 фото</p>
                        </Col>
                        <Col md={9}>
                            <div className={styles.photos_container}>
                                {
                                    photos.map((el, ind) =>
                                        <div
                                            key={ind}
                                            draggable={true}
                                            onDragStart={(e) => handleDragStart(e, el)}
                                            onDragEnd={handleDragEnd}
                                            onDragOver={handleDragOver}
                                            onDrop={(e) => handleDrop(e, el)}
                                            className={styles.image_array_element}
                                        >
                                            <img src={el} style={{ width: 80, height: 80, borderRadius: 8, objectFit: 'cover', cursor: 'grab' }} />
                                            <Icons icon={'cross'} onClick={() => removePhoto(ind)} />
                                        </div>
                                    )
                                }
                                {
                                    photos.length < 20 ?
                                        <Button type={'secondary'} size={'M'} onClick={uploadPhotos}><Icons icon={'add-photo'} className={styles.photo_icon} /></Button>
                                        :
                                        <></>
                                }
                            </div>
                        </Col>
                    </Row>

                </div>

                <div className={styles.block}>
                    <h2 className={styles.subtitle}>Контактная информация</h2>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Адрес</p>
                        </Col>
                        <Col md={4} style={{ position: 'relative' }}>
                            <Input placeholder={"Начните вводить адрес..."} type={"text"} value={address} setValue={setAddress} />
                            {
                                !addressSelected ?
                                    <div className={styles.suggested_addresses}>
                                        {
                                            suggestedAddresses.map((address, index) =>
                                                <div key={index} className={styles.suggested_address} onClick={() => selectAddress(address)}>{address}</div>
                                            )
                                        }
                                    </div>
                                    :
                                    <></>
                            }
                        </Col>
                    </Row>


                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Телефон</p>
                        </Col>
                        <Col md={4}>
                            <Input placeholder={"Номер для бронирования"} type={"phone"} value={phone}
                                setValue={setPhone} />
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Сайт</p>
                        </Col>
                        <Col md={4}>
                            <Input placeholder={"https://..."} type={"text"} value={site} setValue={setSite} />
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Социальные сети</p>
                        </Col>
                        <Col md={4}>
                            <div className={styles.icn_input}><img src={tgIcon} alt={'ВК'} /><Input
                                placeholder={"https://t.me/..."} type={"text"} value={telegram} setValue={setTelegram} />
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className={styles.icn_input}><img src={vkIcon} alt={"ТГ"} /><Input
                                placeholder={"https://vk.com/..."} type={"text"} value={vk} setValue={setVk} />
                            </div>
                        </Col>
                    </Row>


                </div>

                <div className={styles.block}>
                    <h2 className={styles.subtitle}>График работы</h2>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Понедельник</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={MondayStart} setValue={setMondayStart} disabled={MondayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={MondayEnd} setValue={setMondayEnd} disabled={MondayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={MondayOff} setChecked={setMondayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Вторник</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={TuesdayStart} setValue={setTuesdayStart} disabled={TuesdayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={TuesdayEnd} setValue={setTuesdayEnd} disabled={TuesdayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={TuesdayOff} setChecked={setTuesdayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Среда</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={WednesdayStart} setValue={setWednesdayStart} disabled={WednesdayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={WednesdayEnd} setValue={setWednesdayEnd} disabled={WednesdayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={WednesdayOff} setChecked={setWednesdayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Четверг</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={ThursdayStart} setValue={setThursdayStart} disabled={ThursdayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={ThursdayEnd} setValue={setThursdayEnd} disabled={ThursdayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={ThursdayOff} setChecked={setThursdayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Пятница</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={FridayStart} setValue={setFridayStart} disabled={FridayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={FridayEnd} setValue={setFridayEnd} disabled={FridayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={FridayOff} setChecked={setFridayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Суббота</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={SaturdayStart} setValue={setSaturdayStart} disabled={SaturdayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={SaturdayEnd} setValue={setSaturdayEnd} disabled={SaturdayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={SaturdayOff} setChecked={setSaturdayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Воскресенье</p>
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={SundayStart} setValue={setSundayStart} disabled={SundayOff} />
                        </Col>
                        <Col md={2}>
                            <Input type={'time'} value={SundayEnd} setValue={setSundayEnd} disabled={SundayOff} />
                        </Col>
                        <Col md={2}>
                            <Checkbox isChecked={SundayOff} setChecked={setSundayOff}>Выходной день</Checkbox>
                        </Col>
                    </Row>
                </div>

                <div className={styles.block}>
                    <h2 className={styles.subtitle}>Подробности</h2>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Описание</p>
                        </Col>
                        <Col md={8}>
                            <Input placeholder={"Расскажите про концепцию Вашего заведения"} type={"textarea"} value={description} setValue={setDescription} className={styles.description} />
                        </Col>
                    </Row>
                    {
                        // <Row>
                        //     <Col md={3}>
                        //         <p>Средний чек (₽)</p>
                        //     </Col>
                        //     <Col md={4}>
                        //         <Input type={'number'} value={avgBill} setValue={setAvgBill} placeholder='899' />
                        //     </Col>
                        // </Row>
                    }
                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Подходит для случаев</p>
                            <p className={styles.text_secondary}>Отметьте подходящие варианты, чтобы обеспечить гибкий поиск для клиентов</p>
                        </Col>
                        <Col md={9}>
                            {inputVars.SuitableCases.length > 0 ? <MultiSelect items={inputVars.SuitableCases} value={reasons} setValue={setReasons} /> : <></>}
                        </Col>
                    </Row>

                    <Row className={styles.label_input}>
                        <Col md={3}>
                            <p>Дополнительные услуги</p>
                            <p className={styles.text_secondary}>Привлеките пользователей комфортом</p>
                        </Col>
                        <Col md={9}>
                            {inputVars.AdditionalServices.length > 0 ? <MultiSelect items={inputVars.AdditionalServices} value={services} setValue={setServices} /> : <></>}
                        </Col>
                    </Row>
                </div>
                <Button type={"primary"} size={"L"} onClick={createUpdate}>{establishmentId === undefined ? 'Создать' : 'Сохранить'}</Button>
            </div>
        </div>
    )
}

export default withOfferToSignIn(CreatingRest);
