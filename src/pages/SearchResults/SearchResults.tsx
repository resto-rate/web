import React, { useEffect, useState } from "react";


import styles from "./SearchResults.module.css";
import Button from "../../components/UIKit/Button/Button";
import { Col } from "react-bootstrap";
import Row from "react-bootstrap/Row";
import { searchEstablishments } from "../../api/establishment";
import { useSearchParams } from "react-router-dom";
import RestCard, { iRestCardProps } from "../../components/RestCard/RestCard";
import Icons from "../../components/UIKit/Icons";

import { getInputVars } from "../../api/establishment";
import { inputVarsType } from "../CreatingRest/CreatingRest";
import Checkbox from "../../components/UIKit/Checkbox/Checkbox";
import {useUserContext} from "../../context/userContext";
import EmptyStates from "../../components/EmptyStates/EmptyStates";

const select = (value: string[], setValue: (value: string[]) => void, el: string) => {
    const newValue = structuredClone(value)
    if (value.indexOf(el) === -1) {
        newValue.push(el)
    } else {
        newValue.splice(newValue.indexOf(el), 1)
    }
    setValue(newValue)
}

const SearchResults = () => {
    const { user } = useUserContext();
    const [searchParams] = useSearchParams()

    let s = searchParams.get('s') || ''

    const [searchVars, setSearchVars] = useState<inputVarsType | null>(null)
    const [types, setTypes] = useState<string[]>([]);
    const [cuisines, setCuisines] = useState<string[]>([]);
    const [reasons, setReasons] = useState<string[]>(s.length > 0 ? s.split(',').map(ss => decodeURI(ss)) : []);
    const [services, setServices] = useState<string[]>([]);
    console.log(services);

    let query = searchParams.get('q') || ''
    //const [query, setQuery] = useState(searchParams.get('q'))

    useEffect(() => {
        const fetchData = async () => {
            await getInputVars().then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
            }).then(body => {
                if (body) {
                    console.log(body)
                    setSearchVars(body)
                }
            })
        }
        fetchData()
    }, [])

    const [rests, setRests] = React.useState<{ Count: number, Establishments: iRestCardProps[] }>({
        Count: 0,
        Establishments: []
    });

    const searchRests = async (query: string) => {
        let params: {
            fulltextfind: string,
            city: string,
            types?: string,
            suitablecases?: string,
            additionalservices?: string
        } = {fulltextfind: query, city: user.City}
        if (types.length > 0) {
            params.types = types.toString()
        }
        if (reasons.length > 0) {
            params.suitablecases = reasons.toString()
        }
        if(services.length > 0) {
            params.additionalservices = services.toString()
        }
        await searchEstablishments(0, params).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                setRests(body)
                console.log(body)
            }
        })

    }

    useEffect(() => {
        if (query === null) {
            searchRests('')
        } else {
            searchRests(query)
        }
    }, [query])

    return (
        <div className={styles.container}>
            {rests.Count !== 0 ?
                <>
                <h1 className={styles.title}>{query === "" ? "Заведения  в городе" : `Заведения по запросу «${query}»`}</h1>
            <Row className={styles.filters_content}>
                <Col className={styles.filters} md={3}>
                    <h2>Фильтры</h2>

                    <div className={styles.filter_item}>
                        <div className={styles.filter_title}>
                            <p>Формат заведения</p>
                            {/*<Icons icon={'chevron-down'} />*/}
                        </div>
                        <div className={styles.filter_options}>
                            {searchVars?.EstablishmentTypes.map((type, index) => (
                                <Checkbox isChecked={types.indexOf(type) !== -1}
                                    setChecked={() => select(types, setTypes, type)}>{type}</Checkbox>
                            ))}
                        </div>
                    </div>

                    {/*<div className={styles.filter_item}>*/}
                    {/*    <div className={styles.filter_title}>*/}
                    {/*        <p>Вид кухни</p>*/}
                    {/*        <Icons icon={'chevron-down'} />*/}
                    {/*    </div>*/}
                    {/*    <div className={styles.filter_options}>*/}
                    {/*        {searchVars?.CuisineTypes.map((type, index) => (*/}
                    {/*            <Checkbox isChecked={cuisines.indexOf(type) !== -1}*/}
                    {/*                setChecked={() => select(cuisines, setCuisines, type)}>{type}</Checkbox>*/}
                    {/*        ))}*/}
                    {/*    </div>*/}
                    {/*</div>*/}

                    <div className={styles.filter_item}>
                        <div className={styles.filter_title}>
                            <p>Подходящий случай</p>
                            {/*<Icons icon={'chevron-down'} />*/}
                        </div>
                        <div className={styles.filter_options}>
                            {searchVars?.SuitableCases.map((reason, index) => (
                                <Checkbox isChecked={reasons.indexOf(reason) !== -1}
                                    setChecked={() => select(reasons, setReasons, reason)}>{reason}</Checkbox>
                            ))}
                        </div>
                    </div>

                    <div className={styles.filter_item}>
                        <div className={styles.filter_title}>
                            <p>Дополнительные услуги</p>
                            {/*<Icons icon={'chevron-down'} />*/}
                        </div>
                        <div className={styles.filter_options}>
                            {searchVars?.AdditionalServices.map((service, index) => (
                                <Checkbox isChecked={services.indexOf(service) !== -1}
                                    setChecked={() => select(services, setServices, service)}>{service}</Checkbox>
                            ))}
                        </div>
                    </div>


                    <Button type={"secondary"} size={"L"} onClick={() => searchRests(query)}>Применить</Button>
                </Col>
                <Col md={9} className={styles.content}>
                    <div className={styles.sort}>
                        <Icons icon={"sort-down"} />
                        <p>По рейтингу</p>
                        <p>По среднему чеку</p>
                    </div>
                    <div className={styles.rests}>
                        {
                            rests.Establishments.map((el, ind) => <RestCard key={ind} {...el} />)
                        }
                    </div>
                </Col>
            </Row>
                </>
                : <EmptyStates type={"not found"} heading={'Не нашли заведения по Вашему запросу'} text={'Попробуте найти по типу кухни или категориям меню'}/>}
        </div>
    )
}

export default SearchResults;
