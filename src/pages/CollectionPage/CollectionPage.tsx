import React, {useEffect, useState} from "react";
import Row from "react-bootstrap/Row";

import {useNavigate, useParams, useSearchParams} from "react-router-dom";

import Icons from "../../components/UIKit/Icons";

import styles from "./CollectionPage.module.css";
import Modal from "../../components/Modal/Modal";
import EditCollection from "../../components/EditCollection/EditCollection";
import DeleteCollection from "../../components/DeleteCollection/DeleteCollection";
import {useAlertContext} from "../../context/alertContext";
import {copyCurrentHref, switchEnding} from "../../utils/Functions";
import Button from "../../components/UIKit/Button/Button";
import {ICollection, useCollectionsContext} from "../../context/CollectionsContext";
import {getCollection} from "../../api/establishment";
import RestCard, {iRestCardProps} from "../../components/RestCard/RestCard";


const CollectionPage: React.FC = () => {
    const {collections} = useCollectionsContext();
    const navigate = useNavigate();
    const [openEditCollection, setOpenEditCollection] = useState(false);
    const [openDeleteCollection, setOpenDeleteCollection] = useState(false);
    const {setAlert} = useAlertContext();
    const [collection, setCollection] = useState<ICollection & {Establishments: iRestCardProps[]}>();
    const [searchParams] = useSearchParams()
    const params = useParams()
    const collectiontId = params.id ? params.id : searchParams.get('id')

    const copyAndAlert = () => {
        copyCurrentHref()
        setAlert({
            icon: "share",
            message: "Ссылка на коллекцию скопирована в буфер обмена. Поделитесь с друзьями вашей подборкой"
        })
    }

    const fetchCollection = async () => {
        if (collectiontId === null) {
            return
        }
        await getCollection(collectiontId).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
                console.log(body);
                setCollection(body);
            }
        )
    }

    useEffect(() => {
        fetchCollection()
    }, []);

    return (
        <>
            <div className={styles.back} onClick={() => navigate(-1)}>
                <Icons icon={'back'}/>
                <p>Назад</p>
            </div>

            {collection !== undefined &&
                <div className={styles.title_actions}>
                    <div className={styles.title_amount}>
                        <div className={styles.title} onClick={() => {
                            setOpenEditCollection(true)
                        }}>
                            <p>{collection.Title}</p>
                            <Icons icon={"edit"} className={styles.icon_edit}/>
                        </div>
                        <p className={styles.amount}>{collection.Establishments.length} {switchEnding(['мест', 'место', 'мест'], collection.Establishments.length)}</p>
                    </div>
                    <div className={styles.actions}>
                        <Button type={"secondary"} size={"M"} onClick={() => {
                            setOpenDeleteCollection(true)
                        }}><Icons icon={"delete"} className={styles.icon_delete}/></Button>
                        <Button type={"secondary"} size={"M"} onClick={copyAndAlert}><Icons icon={"share"}
                                                                                            className={styles.icon_share}/></Button>
                    </div>
                </div>
            }


            <Row className={styles.cards}>
                {collection?.Establishments.map((est, ind) =>
                    <RestCard {...est} key={ind}/>)}
            </Row>

            <Modal open={openEditCollection} setOpen={setOpenEditCollection}>
                <EditCollection name={'Название коллекции'} id={'id'}/>
            </Modal>

            <Modal open={openDeleteCollection} setOpen={setOpenDeleteCollection}>
                <DeleteCollection name={'Название коллекции'} id={'id'}/>
            </Modal>
        </>
    )
}

export default CollectionPage;
