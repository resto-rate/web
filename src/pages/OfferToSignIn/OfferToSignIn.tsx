import React from 'react';

import styles from './OfferToSignIn.module.css';
import Button from "../../components/UIKit/Button/Button";

import img from "./OfferToSignIn.svg";
import {openAuth} from "../../utils/Functions";

const OfferToSignIn = () => {
    return (
        <div className={styles.container}>
            <img src={img} alt={"offer to sign in"} />
            <div className={styles.text_button}>
                <h1>Войдите или зарегистрируйтесь</h1>
                <p>Чтобы оставлять отзывы, составлять коллекции и добавлять собственные заведения</p>
                <div className={styles.button_container}>
                    <Button type={"primary"} size={"L"} onClick={openAuth}>Вход/Регистрация</Button>
                </div>
            </div>
        </div>
    )
}

export default OfferToSignIn;
