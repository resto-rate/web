import React from 'react'
import { Col, Row } from 'react-bootstrap';
import { Outlet, useNavigate, useParams } from 'react-router-dom';
import Button from '../../components/UIKit/Button/Button';
import NavigationBar from '../../components/NavigationBar/NavigationBar';

function MyRest() {
    const params = useParams()
    const navigate = useNavigate()
    return (
        <>
            <NavigationBar/>
            <Outlet />
        </>
    )
}

export default MyRest;
