import React, {useEffect, useState} from "react";
import {Col, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";

import { createUpdateEvent, createUpdatePromotion, deleteEvent, deletePromotion,
    getEstablishmentEvents, getEstablishmentPromotions, uploadImage} from "../../api/establishment";
import Button from "../../components/UIKit/Button/Button";
import Modal from "../../components/Modal/Modal";
import Input from "../../components/UIKit/Input/Input";
import Icons from "../../components/UIKit/Icons";
import Checkbox from "../../components/UIKit/Checkbox/Checkbox";
import {iRestPromotion, PromotionCard} from "../../components/PromotionCard/PromotionCard";
import EventCard, {iRestEvent} from "../../components/EventCard/EventCard";
import {useUserContext} from "../../context/userContext";
import {useAlertContext} from "../../context/alertContext";
import {dateToNumber, numberToDate, numberToTime, timeToNumber} from "../../utils/Functions";
import styles from "./UpdatingPromotionsEvents.module.css";

interface iPromotions {
    Count: number,
    Promotions: iRestPromotion[]
}

interface iEvents {
    Count: number,
    Events: iRestEvent[]
}

const UpdatingPromotionsEvents = () => {
    const {user} = useUserContext()
    const { setAlert } = useAlertContext()
    const params = useParams();

    const [isOpenPromotion, setIsOpenPromotion] = useState(false)
    const [promotions, setPromotions] = useState<iPromotions>({Count: 0, Promotions: []})

    const [isOpenEvent, setIsOpenEvent] = useState(false)
    const [events, setEvents] = useState<iEvents>({Count: 0, Events: []})

    const [title, setTitle] = useState<string>('') //promotions and Events
    const [image, setImage] = useState<string>('') //promotions and Events
    const [conditions, setConditions] = useState<string>('') //promotions
    const [startDate, setStartDate] = useState<string>('') //promotions and Events
    const [endDate, setEndDate] = useState<string>('') //promotions
    const [time, setTime] = useState<string>('') //Events
    const [undying, setUndying] = useState<boolean>(false) //promotions
    const [contactInfo, setContactInfo] = useState<string>('') //promotions and Events
    const [description, setDescription] = useState<string>('') //Events

    const [renewingPromotion, setRenewingPromotion] = useState<string | null>(null)
    const [renewingEvent, setRenewingEvent] = useState<string | null>(null)

    const establishmentId = params.id

    useEffect(() => {
        console.log(startDate)
    }, [startDate])

    const fetchData = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishmentPromotions(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setPromotions(body)
            }
        })
        await getEstablishmentEvents(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setEvents(body)
            }
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    const clearInputs = () => {
        setTitle('')
        setImage('')
        setConditions('')
        setStartDate('')
        setEndDate('')
        setTime('')
        setUndying(false)
        setContactInfo('')
        setDescription('')
        setRenewingPromotion(null)
        setRenewingEvent(null)
    }

    useEffect(() => {
        if (!isOpenPromotion && !isOpenEvent) {
            clearInputs()
        }
    }, [isOpenPromotion, isOpenEvent])

    const promotionToModal = (promotion: iRestPromotion) => {
        setTitle(promotion.Title)
        setImage(promotion.Image)
        setConditions(promotion.Conditions)
        setStartDate(numberToDate(promotion.StartTime * 1000))
        setEndDate(numberToDate(promotion.EndTime * 1000))
        setUndying(promotion.Undying)
        setContactInfo(promotion.ContactInfo)
        setIsOpenPromotion(true)
    }

    const updatePromotion = async () => {
        if (establishmentId === null) {
            return
        }
        let body: any = {
            Title: title,
            Image: image,
            Conditions: conditions,
            StartTime: dateToNumber(startDate),
            EndTime: dateToNumber(endDate),
            ContactInfo: contactInfo,
            Undying: undying,
        }
        if (renewingPromotion !== null) {
            body = {...body, PromotionGUID: renewingPromotion}
        }
        await createUpdatePromotion(body, establishmentId, user.At.Token).then(resp => {
            if (resp.ok) {
                console.log("Promotion Created");
                return resp.json()
            }
        }).then((body: iRestPromotion) => {
            if (body) {
                console.log(body)
                const newPromotions: iPromotions = structuredClone(promotions)
                if (renewingPromotion) {
                    newPromotions.Promotions.forEach(p => {
                        if (p.PromotionGUID == body['PromotionGUID']) {
                            p.Title = body.Title;
                            p.Image = body.Image;
                            p.Conditions = body.Conditions;
                            p.StartTime = body.StartTime;
                            p.EndTime = body.EndTime;
                            p.Undying = body.Undying;
                            p.ContactInfo = body.ContactInfo;
                        }
                    })
                } else {
                    newPromotions.Promotions.push(body)
                    newPromotions.Count += 1
                }
                setPromotions(newPromotions)
            }
        setIsOpenPromotion(false)
        }).catch(err => {
            console.log(err)
            return false
        })
    }

    const deletePromo = async () => {
        if (establishmentId === undefined || renewingPromotion === null) {
            return
        }
        if (promotions.Promotions.length > 0) {
            await deletePromotion(renewingPromotion, establishmentId, user.At.Token).then(resp => {
                console.log(resp)
                if (resp.ok) {
                    const newPromotions: iPromotions = structuredClone(promotions)
                    promotions.Promotions.forEach((p, ind) => {
                        if (p.PromotionGUID === renewingPromotion) {
                            newPromotions.Promotions.splice(ind, 1)
                        }
                    })
                    setPromotions(newPromotions)
                    setIsOpenPromotion(false)
                }
            })
        }
    }

    const eventToModal = (event: iRestEvent) => {
        setTitle(event.Title)
        setImage(event.Image)
        setDescription(event.Description)
        setStartDate(numberToDate(event.DateTime * 1000))
        setTime(numberToTime(event.DateTime * 1000))
        setContactInfo(event.ContactInfo)
        setIsOpenEvent(true)
    }

    const updateEvent = async () => {
        if (establishmentId !== null) {
            let body: any = {
                Title: title,
                Image: image,
                Description: description,
                DateTime: dateToNumber(startDate) + timeToNumber(time),
                ContactInfo: contactInfo,
            }
            if (renewingEvent !== null) {
                body = {...body, EventGUID: renewingEvent}
            }
            await createUpdateEvent(body, establishmentId, user.At.Token).then(resp => {
                if (resp.ok) {
                    console.log("Event Created");
                    return resp.json()
                }
            }).then((body: iRestEvent) => {
                if (body) {
                    console.log(body)
                    const newEvents: iEvents = structuredClone(events)
                    if (renewingEvent) {
                        newEvents.Events.forEach(e => {
                            if (e.EventGUID == body['EventGUID']) {
                                e.Title = body.Title;
                                e.Image = body.Image;
                                e.Description = body.Description;
                                e.DateTime = body.DateTime;
                                e.ContactInfo = body.ContactInfo;
                            }
                        })
                    } else {
                        newEvents.Events.push(body)
                        newEvents.Count += 1
                    }
                    setEvents(newEvents)
                }
                setIsOpenEvent(false)
            }).catch(err => {
                console.log(err)
                return false
            })
        }
    }

    const deleteEventEl = async () => {
        if (establishmentId === undefined || renewingEvent === null) {
            return
        }
        if (events.Events.length > 0) {
            await deleteEvent(renewingEvent, establishmentId, user.At.Token).then(resp => {
                console.log(resp)
                if (resp.ok) {
                    const newEvent: iEvents = structuredClone(events)
                    events.Events.forEach((e, ind) => {
                        if (e.EventGUID === renewingEvent) {
                            newEvent.Events.splice(ind, 1)
                        }
                    })
                    setEvents(newEvent)
                    setIsOpenEvent(false)
                }
            })
        }
    }

    const upload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            console.log("Uploading file...");

            const formData = new FormData();
            // @ts-ignore
            formData.append("imageFile", e.target.files[0]);
            let result = null
            try {
                result = await uploadImage(formData, user.At.Token)
            } catch (error) {
                console.log(error)
                setAlert({
                    icon: "attention",
                    message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 10МБ"
                });
            }
            if (result === null)
                return
            if (result.ok) {
                const data = await result.json();
                setImage(data)
                //@ts-ignore
                document.getElementById('photo_input').value = ''
            } else {
                console.log(result);
                switch (result.status) {
                    case 413:
                        setAlert({
                            icon: "attention",
                            message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 10МБ"
                        });
                }
            }
        }
    }

    return (
        <div className={styles.container}>
            <h1 className={styles.title}>Управление акциями и мероприятиями</h1>
            <input type='file' id='photo_input' hidden onChange={upload} />
            <div className={styles.block}>
                <Row>
                    <Col style={{display: 'flex'}}>
                        <h1 className={styles.subtitle}>Активные акции</h1>
                        <div style={{flex: 1}}/>
                        <Button type='primary' size='L' onClick={() => setIsOpenPromotion(true)}>Добавить</Button>
                    </Col>
                </Row>
                <Row classname={styles.promotions}>
                    {
                        promotions.Promotions.map((el, ind) =>
                            <Col md={3} >
                                <PromotionCard promotion={el} onClick={() => {setRenewingPromotion(el.PromotionGUID);promotionToModal(el)}}/>
                            </Col>
                        )
                    }
                </Row>
            </div>
            <Modal open={isOpenPromotion} setOpen={setIsOpenPromotion}>
                <div className={styles.promotion_modal}>
                    <h2 className={styles.modal_title}>Акционное предложение</h2>
                    <div className={styles.modal_content}>
                        <div className={styles.preview}>
                            <img className={styles.promotion_preview} src={image ? image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/promotion_placeholder.jpg'}/>
                        </div>
                        <div className={styles.modal_inputs}>
                            <Input type='text' placeholder='Название' value={title} setValue={setTitle}/>
                            <Button type='secondary' size='M' onClick={() => document.getElementById('photo_input')?.click()}>
                                <Icons className={styles.photo_icon} icon='photo'/>
                                Превью&nbsp;акции
                            </Button>
                            <div className={styles.flex_row}>
                                <Input type={"date"} value={startDate} setValue={setStartDate} disabled={undying}/>
                                <Input type={"date"} value={endDate} setValue={setEndDate} disabled={undying}/>
                            </div>
                            <Checkbox isChecked={undying} setChecked={setUndying}>Бессрочно</Checkbox>
                            <Input type='textarea' placeholder='Условия акции' value={conditions}
                                   setValue={setConditions}
                                   className={styles.conditions}/>
                            <Input type='text' placeholder='Контактная информация' value={contactInfo}
                                   setValue={setContactInfo}/>
                        </div>
                    </div>
                    <div className={styles.save_btn}>
                        <Button type={'secondary'} size={'L'} onClick={deletePromo}>Удалить</Button>
                        <div/>
                        <Button type='primary' size='L' onClick={updatePromotion}>Сохранить</Button>
                    </div>
                </div>
            </Modal>
            <div className={styles.block}>
                <Row>
                    <Col style={{display: 'flex'}}>
                        <h1 className={styles.subtitle}>Запланированные мероприятия</h1>
                        <div style={{flex: 1}}/>
                        <Button type='primary' size='L' onClick={() => setIsOpenEvent(true)}>Добавить</Button>
                    </Col>
                </Row>
                <Row>
                    {
                        events.Events.map((el, ind) =>
                            <Col md={6}>
                                <EventCard event={el} onClick={() => {eventToModal(el); setRenewingEvent(el.EventGUID)}}/>
                            </Col>
                        )
                    }
                </Row>
            </div>
            <Modal open={isOpenEvent} setOpen={setIsOpenEvent}>
                <div className={styles.promotion_modal}>
                    <h2 className={styles.modal_title}>Мероприятие</h2>
                    <div className={styles.modal_content}>
                        <div className={styles.preview}>
                            <img className={styles.event_photo}
                                 src={image ? image : 'https://restorate.hb.ru-msk.vkcs.cloud/static/event_placeholder.jpg'}/>
                        </div>
                        <div className={styles.modal_inputs}>
                            <Input type='text' placeholder='Название' value={title} setValue={setTitle}/>
                            <Button type='secondary' size='M' onClick={() => document.getElementById('photo_input')?.click()}>
                                <Icons className={styles.photo_icon} icon='photo'/>
                                Фотография
                            </Button>
                            <div className={styles.flex_row}>
                                <Input type={"date"} value={startDate} setValue={setStartDate}/>
                                <Input type={"time"} value={time} setValue={setTime}/>
                            </div>
                            <Input type='textarea' placeholder='Описание мероприятия' value={description}
                                   setValue={setDescription} className={styles.conditions}/>
                            <Input type='text' placeholder='Контактная информация' value={contactInfo}
                                   setValue={setContactInfo}/>
                        </div>
                    </div>
                    <div className={styles.save_btn}>
                        <Button type='secondary' size='L' onClick={deleteEventEl}>Удалить</Button>
                        <div/>
                        <Button type='primary' size='L' onClick={updateEvent}>Сохранить</Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default UpdatingPromotionsEvents;
