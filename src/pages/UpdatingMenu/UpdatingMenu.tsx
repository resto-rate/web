import React, { useEffect, useState } from 'react';

import Button from "../../components/UIKit/Button/Button";
import Modal from "../../components/Modal/Modal";
import { Link, useParams, useSearchParams } from "react-router-dom";
import { createUpdateMeal, deleteMeal, getInputVars, getMenu, uploadImage } from "../../api/establishment";
import { iMenu } from "../Menu/Menu";
import Row from "react-bootstrap/Row";
import { Col } from "react-bootstrap";
import MealCard, { iMeal } from "../../components/Rest/MealCard/MealCard";
import Select from "../../components/UIKit/Select/Select";
import Input from "../../components/UIKit/Input/Input";
import styles from './UpdatingMenu.module.css';
import { useUserContext } from '../../context/userContext';
import rest from "../Rest/Rest";
import Icons from '../../components/UIKit/Icons';
import { useAlertContext } from '../../context/alertContext';


type inputVarsType = {
    EstablishmentTypes: string[]
    DishCategories: string[]
    AdditionalServices: string[]
    CuisineTypes: string[]
    SuitableCases: string[]
}


const UpdatingMenu = () => {
    const { user } = useUserContext()
    const { setAlert } = useAlertContext()

    const [isOpenMeal, setIsOpenMeal] = useState(false);
    const [menu, setMenu] = useState<iMenu>();

    const [category, setCategory] = useState("");
    const [mealName, setMealName] = useState("");
    const [price, setPrice] = useState("0");
    const [weight, setWeight] = useState("");
    const [volume, setVolume] = useState("");
    const [composition, setComposition] = useState("");
    const [calories, setCalories] = useState("");
    const [proteins, setProteins] = useState("");
    const [fats, setFats] = useState("");
    const [carbohydrates, setCarbohydrates] = useState("");
    const [weightOrVolume, setWeightOrVolume] = useState("г");
    const [dishImage, setDishImage] = useState("");
    const [renewingMeal, setRenewingMeal] = useState<string | null>(null)
    const [searchParams, setSearchParams] = useSearchParams();
    const params = useParams();

    const clearInputs = () => {
        setCategory('')
        setMealName('')
        setPrice('')
        setWeight('')
        setVolume('')
        setComposition('')
        setCalories('')
        setProteins('')
        setFats('')
        setCarbohydrates('')
        setWeightOrVolume('г')
        setDishImage('')
        setRenewingMeal(null)
    }

    const establishmentId = params.id ? params.id : searchParams.get('id');

    const [inputVars, setInputVars] = useState<inputVarsType>({
        EstablishmentTypes: [],
        DishCategories: [],
        AdditionalServices: [],
        CuisineTypes: [],
        SuitableCases: []
    });

    const fetchMenu = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getMenu(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setMenu({ ...body })
            } else {
                console.log('Wrong establishment id')
            }
            console.log(menu)
        })
    }
    useEffect(() => {
        fetchMenu()
    }, [])

    const updateMeal = async () => {
        if (establishmentId === null) {
            return
        }
        let body: any = {
            Category: category,
            Price: Number(price),
            Weight: Number(weight),
            Volume: Number(volume),
            DishImage: dishImage,
            DishName: mealName,
            Composition: composition,
            Calories: Number(calories),
            Proteins: Number(proteins),
            Fats: Number(fats),
            Carbohydrates: Number(carbohydrates)
        }
        if (renewingMeal !== null) {
            body = { ...body, DishGUID: renewingMeal }
        }
        await createUpdateMeal(body, establishmentId, user.At.Token).then(resp => {
            if (resp.ok) {
                console.log("Meal Created");
                return resp.json()
            }
        }).then((body: iMeal) => {
            if (body) {
                console.log(body)
                if (menu !== undefined && menu.DishesByCategory !== null) {
                    const newMenu: iMenu = structuredClone(menu)
                    let categoryPresent = false
                    newMenu.DishesByCategory.forEach(c => {
                        if (c.Category === body.Category) {
                            categoryPresent = true
                            if (renewingMeal) {
                                c.Dishes.forEach(m => {
                                    if (m.DishGUID == body['DishGUID']) {
                                        m.Calories = body.Calories
                                        m.Carbohydrates = body.Carbohydrates
                                        m.Category = body.Category
                                        m.Composition = body.Composition
                                        m.DishImage = body.DishImage
                                        m.DishName = body.DishName
                                        m.Fats = body.Fats
                                        m.Price = body.Price
                                        m.Proteins = body.Proteins
                                        m.Volume = body.Volume
                                        m.Weight = body.Weight
                                    }
                                })
                            } else {
                                c.Dishes.push(body)
                            }
                        }
                    })
                    if (!categoryPresent) {
                        newMenu.DishesByCategory.push({ Category: body.Category, Dishes: [body] })
                    }
                    if (!renewingMeal) {
                        newMenu.Count += 1
                    }
                    setMenu(newMenu)
                } else {
                    setMenu({ Count: 1, DishesByCategory: [{ Category: body.Category, Dishes: [body] }] })
                }
            }
        }).catch(err => {
            console.log(err)
            return false
        })
    }

    const deleteDish = async () => {
        if (establishmentId === null || renewingMeal === null) {
            return
        }
        if (menu !== undefined && menu.DishesByCategory !== null) {
            const newMenu: iMenu = structuredClone(menu)
            newMenu.DishesByCategory.forEach(c => {
                c.Dishes.forEach((d, ind) => {
                    if (d.DishGUID === renewingMeal) {
                        c.Dishes.splice(ind, 1)
                    }
                })
            })
            setMenu(newMenu)
            await deleteMeal(establishmentId, renewingMeal, user.At.Token).then(resp => {
                console.log(resp)
            })
        }
    }

    useEffect(() => {
        const fetchVars = async () => {
            await getInputVars().then(resp => {
                if (resp.ok) {
                    return resp.json()
                }
                return null
            }).then(body => {
                console.log(body)
                if (body) {
                    setInputVars(body)
                }
            })
        }
        fetchVars()
    }, []);

    useEffect(() => {
        if (!isOpenMeal) {
            clearInputs()
        }
    }, [isOpenMeal])

    const mealToModal = (meal: iMeal) => {
        setCategory(meal.Category);
        setMealName(meal.DishName);
        setCalories(meal.Calories.toString());
        setCarbohydrates(meal.Carbohydrates.toString());
        setComposition(meal.Composition);
        setFats(meal.Fats.toString());
        setDishImage(meal.DishImage);
        setPrice(meal.Price.toString());
        setProteins(meal.Proteins.toString());
        setWeight(meal.Weight.toString());
        setVolume(meal.Volume.toString());
        setIsOpenMeal(true);
    }

    const upload = async (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            console.log("Uploading file...");

            const formData = new FormData();
            // @ts-ignore
            formData.append("imageFile", e.target.files[0]);
            let result = null
            try {
                result = await uploadImage(formData, user.At.Token)
            } catch (error) {
                console.log(error)
                setAlert({
                    icon: "attention",
                    message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 10МБ"
                });
            }
            if (result === null)
                return
            if (result.ok) {
                const data = await result.json();
                setDishImage(data)
                //@ts-ignore
                document.getElementById('photo_input').value = ''
            } else {
                console.log(result);
                switch (result.status) {
                    case 413:
                        setAlert({
                            icon: "attention",
                            message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 10МБ"
                        });
                }
            }
        }
    }

    if (menu === undefined) {
        return (<div>Загрузка...</div>)
    }

    return (
        <div>
            <input type='file' id="photo_input" hidden onChange={upload} />
            <div className={styles.title_button}>
                <h1>Редактирование меню</h1>
                <Button type={"primary"} size={"L"} onClick={() => setIsOpenMeal(true)}>
                    Добавить позицию
                </Button>
            </div>
            {
                menu.Count > 0 ?
                    menu.DishesByCategory.sort((a, b) => a.Category.localeCompare(b.Category)).map((cat, indCat) =>
                        <div key={indCat} className={styles.category_block}>
                            <Row>
                                <h2 className={styles.category}>{cat.Category}</h2>
                            </Row>
                            <Row>
                                {
                                    cat.Dishes.map((dish, indDish) =>
                                        <Col md={3} key={indDish}>
                                            <div className={styles.meal} onClick={() => { mealToModal(dish); setRenewingMeal(dish.DishGUID) }}><MealCard meal={dish} /></div>
                                        </Col>
                                    )
                                }
                            </Row>
                        </div>)
                    :
                    <></>
            }
            <Modal open={isOpenMeal} setOpen={setIsOpenMeal}>
                <div className={styles.container}>
                    <h1>Позиция меню</h1>
                    <div className={styles.main}>
                        <div className={styles.preview}>
                            <MealCard meal={{
                                Calories: Number(calories), Carbohydrates: Number(carbohydrates),
                                Category: category, Composition: composition, DishGUID: '', DishImage: dishImage, DishName: mealName,
                                EstablishmentGUID: '', Fats: Number(fats), LikeCount: 0, Price: Number(price), Proteins: Number(proteins),
                                Volume: Number(volume), Weight: Number(weight)
                            }} />
                        </div>
                        <div className={styles.inputs}>
                            <div className={styles.photo}></div>
                            <Select value={category} setValue={setCategory} datalist={inputVars.DishCategories} placeholder={"Категория"} />
                            <Input type={"text"} value={mealName} setValue={setMealName} placeholder={"Название позиции"} />

                            <div className={styles.group}>
                                <Button type={'secondary'} size={'S'} onClick={() => document.getElementById('photo_input')?.click()}><Icons icon={'photo'} className={styles.photo_button} /></Button>
                                <Input type={"number"} value={price} setValue={setPrice} placeholder={"Цена"} />
                                {
                                    weightOrVolume === "г" ?
                                        <Input type={"number"} value={weight} setValue={setWeight} placeholder={"Вес"} />
                                        :
                                        <Input type={"number"} value={volume} setValue={setVolume} placeholder={"Объем"} />
                                }
                                <Select value={weightOrVolume} setValue={setWeightOrVolume} datalist={["г", "мл"]} />
                            </div>

                            <Input type={"text"} value={composition} setValue={setComposition} placeholder={"Состав"} />

                            <div className={styles.group}>
                                <Input type={"number"} value={calories} setValue={setCalories} placeholder={"ККал"} />
                                <Input type={"number"} value={proteins} setValue={setProteins} placeholder={"Белки"} />
                                <Input type={"number"} value={fats} setValue={setFats} placeholder={"Жиры"} />
                                <Input type={"number"} value={carbohydrates} setValue={setCarbohydrates} placeholder={"Углеводы"} />
                            </div>
                        </div>
                    </div>

                    <div className={styles.buttons}>
                        {
                            renewingMeal ?
                                <Button type={"secondary"} size={"M"} onClick={() => { deleteDish(); setIsOpenMeal(false) }}>Удалить из меню</Button>
                                :
                                <div></div>
                        }
                        <Button type={"primary"} size={"M"} onClick={() => { updateMeal(); setIsOpenMeal(false) }}> Сохранить </Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default UpdatingMenu;
