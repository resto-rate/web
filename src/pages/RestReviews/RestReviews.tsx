import React, {useState, useEffect} from "react";
import styles from "./RestReviews.module.css";
import RestHeader from "../../components/Rest/RestHeader/RestHeader";
import RatingCriterion from "../../components/RatingCriterion/RatingCriterion";
import {switchCriterion, switchEnding} from "../../utils/Functions";
import Button from "../../components/UIKit/Button/Button";
import Row from "react-bootstrap/Row";
import {Col} from "react-bootstrap";
import MealCard from "../../components/Rest/MealCard/MealCard";
import Stars from "../../components/UIKit/Stars/Stars";
import NewReview from "../../components/NewReview/NewReview";
import Modal from "../../components/Modal/Modal";
import {getEstablishment, getEstablishmentReviews} from "../../api/establishment";
import {useParams, useSearchParams} from "react-router-dom";
import {iRest} from "../Rest/Rest";
import Review, {iReviews} from "../../components/Review/Review";


const RestReviews = () => {
    const [rest, setRest] = useState<iRest | null>(null)
    const [searchParams] = useSearchParams()
    const params = useParams()
    const establishmentId = params.id ? params.id : searchParams.get('id')
    const [reviews, setReviews] = useState<iReviews>({Count: 0, Reviews: []})


    const [openNewReview, setOpenNewReview] = useState<boolean>(false);

    const fetchEstablishment = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setRest({...body})
            } else {
                console.log('Wrong establishment id')
            }
            console.log(rest)
        })

        await getEstablishmentReviews(establishmentId).then(resp => {
            if (resp.ok) {
                console.log(resp)
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setReviews(body)
            }
        })
    }

    useEffect(() => {
        fetchEstablishment()
    }, []);


    return (
        <>
            <RestHeader/>



            {rest !== null
                ?

                <>

                    <Row className={styles.rates}>
                        <Col md={2} className={styles.rating_overall}>
                            <span>{rest.Rating.AvgRating}</span>
                            <div>
                                <Stars rating={rest.Rating.AvgRating} size={'s18'}/>
                                <p>{rest.Rating.ReviewCount} {switchEnding(['отзывов', 'отзыв', 'отзывов'], rest.Rating.ReviewCount)}</p>
                            </div>

                        </Col>

                        {Object.keys(rest.Rating.AvgRates).map((criterion, index) =>
                            // @ts-ignore
                            <Col md={2}><RatingCriterion criterion={switchCriterion(criterion.toLowerCase())} rating={rest.Rating.AvgRates[criterion]} key={index} /></Col>)}
                    </Row>

                    <Row className={styles.reviews} style={{marginTop: '54px'}}>
                        <div className={styles.title_btn}>
                            <h2>Отзывы посетителей</h2>
                            <Button type={"primary"} size={"L"} onClick={() => setOpenNewReview(true)}>Оставить отзыв</Button>
                        </div>
                        {reviews.Reviews.map((r, ind) => <Review review={r} type={'rest'} onDelete={() => 0} key={ind}/>)}
                    </Row>

                </>

            :
                <></>
            }



            <Modal open={openNewReview} setOpen={setOpenNewReview}>
                <NewReview onComplete={fetchEstablishment}/>
            </Modal>

        </>
    )
}

export default RestReviews;