import React from 'react';

import styles from './Page404.module.css';
import Button from "../../components/UIKit/Button/Button";
import {useNavigate} from "react-router-dom";
import img404 from "./404.svg";

const Page404 = () => {
    const navigate = useNavigate();
    return (
        <div className={styles.container}>
            <h1>Cтраница не найдена</h1>
            <img src={img404} alt={"page not found"}/>
            <Button type={"primary"} size={"L"} onClick={() => navigate("/")}>Перейти к главной</Button>
        </div>
    )
}

export default Page404;
