import React, {useState} from "react";
import {Outlet} from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Tabs, {iTabsProps} from '../../components/UIKit/Tabs/Tabs';

import styles from './Profile.module.css';
import {useUserContext} from "../../context/userContext";
import {changeProfileImage} from "../../api/auth";
import Icons from "../../components/UIKit/Icons";
import Modal from "../../components/Modal/Modal";
import ChangeName from "../../components/ChangeName/ChangeName";
import {withOfferToSignIn} from "../../hoc/withOfferToSignIn";
import {useAlertContext} from "../../context/alertContext";


const Profile: React.FC = () => {
    const {user, setUser} = useUserContext();
    const {setAlert} = useAlertContext();

    const [photoLoading, setPhotoLoading] = useState(false);
    const [openChangeName, setOpenChangeName] = useState(false);

    const uploadPhoto = () => {
        //@ts-ignore
        document.getElementById('file').click()
    }

    const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        setPhotoLoading(true)
        if (e.target.files) {
            console.log("Uploading file...");

            const formData = new FormData();
            // @ts-ignore
            formData.append("imageFile", e.target.files[0]);
            let result = null
            try {
                // You can write the URL of your server or any other endpoint used for file upload
                result = await changeProfileImage(formData, user.At.Token)
            } catch (error) {
                setPhotoLoading(false)
                console.log(error)
                setAlert({
                    icon: "attention",
                    message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                });
            }
            if (result === null)
                return

            if (result.ok) {
                const data = await result.json();
                setUser({...user, ...data});
                console.log(data);
                setPhotoLoading(false)
            } else {
                console.log(result);
                switch (result.status) {
                    case 413:
                        setAlert({
                            icon: "attention",
                            message: "Выбранная фотография имеет неправильный формат или слишком большой размер. Убедитесь, что файл имеет расширение png или jpg и не превышает 1МБ"
                        });
                }
                setPhotoLoading(false);
            }

        }
    };

    const tabs: iTabsProps["tabs"] = [
        {
            title: 'Подборки',
            link: '/profile/collections'
        },
        {
            title: 'Отзывы',
            link: '/profile/reviews'
        }
    ]

    return (
        <>
            <Row>
                <Col xs={4} md={5}></Col>
                <Col xs={4} md={2}>
                    <div className={styles.avatar_container} onClick={uploadPhoto}>
                        {
                            user.ImageURL ?
                                <img className={styles.avatar} src={user.ImageURL}
                                     alt={`${user.Name.charAt(0)} ${user.Surname.charAt(0)}`}/>
                                :
                                <div className={styles.avatar}>{user.Name.charAt(0)} {user.Surname.charAt(0)}</div>
                        }
                        <div className={styles.icon_change_photo}>
                            <Icons icon={'photo'}/>
                        </div>
                        {
                            photoLoading ?
                                <div className={styles.loader_container}>
                                    <span className={styles.loader}></span>
                                </div>
                                :
                                <></>
                        }
                    </div>
                </Col>
                <Col xs={4} md={5} hidden>
                    <input id='file' type='file' onChange={handleFileChange}/>
                </Col>
            </Row>
            <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                    <div className={styles.edit_name} onClick={() => {
                        setOpenChangeName(true)
                    }}>
                        <p className={styles.name}>{user.Name}&nbsp;{user.Surname}</p>
                        <Icons icon={"edit"} className={styles.edit}/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={2}></Col>
                <Col xs={8}>
                    <Tabs tabs={tabs}/>
                </Col>
            </Row>
            <Outlet/>

            <Modal open={openChangeName} setOpen={setOpenChangeName}>
                <ChangeName/>
            </Modal>

        </>
    )
}

export default withOfferToSignIn(Profile);
