import React, {useEffect, useState} from 'react'
import {Link, useNavigate, useParams, useSearchParams} from 'react-router-dom'
import {Col, Row} from 'react-bootstrap'
import {Map, Placemark, ZoomControl} from '@pbe/react-yandex-maps';
import cn from 'classnames'

import {
    getEstablishment,
    getEstablishmentEvents,
    getEstablishmentPromotions,
    getEstablishmentReviews
} from '../../api/establishment'

import Button from '../../components/UIKit/Button/Button'
import RestHeader from '../../components/Rest/RestHeader/RestHeader'

import styles from './Rest.module.css'
import MealCard, {iMeal} from '../../components/Rest/MealCard/MealCard'
import Icons from '../../components/UIKit/Icons'
import {prettyPhone, prettyTime, switchCriterion, switchEnding} from '../../utils/Functions'
import {iRestPromotion, PromotionCard} from "../../components/PromotionCard/PromotionCard";
import EventCard, {iRestEvent} from "../../components/EventCard/EventCard";
import Stars from "../../components/UIKit/Stars/Stars";
import ChangeName from "../../components/ChangeName/ChangeName";
import Modal from "../../components/Modal/Modal";
import NewReview from "../../components/NewReview/NewReview";
import Review, {iReviews} from "../../components/Review/Review";
import RatingCriterion from "../../components/RatingCriterion/RatingCriterion";


interface scheduleDay {
    OpenTime: number,
    CloseTime: number,
    RestDay: boolean
}


export interface iRest {
    EstablishmentGUID: string
    EstablishmentTitle: string
    Geolocation: {
        Latitude: number
        Longitude: number
    },
    EstablishmentLogo: string
    Type: string
    Cuisines: string[]
    Images: string[]
    Rating: {
        AvgRating: number
        ReviewCount: number
        AvgRates: {
            Service: number
            Food: number
            Vibe: number
            PriceQuality: number
            WaitingTime: number
        }
        Images: string[]
    }
    AvgBill: number
    Description: string
    Address: string
    Contacts: {
        MobilePhone: string
        SiteURL: string
        WhatsApp: string
        Telegram: string
        VK: string
    }
    Schedule: {
        Monday: scheduleDay
        Tuesday: scheduleDay
        Wednesday: scheduleDay
        Thursday: scheduleDay
        Friday: scheduleDay
        Saturday: scheduleDay
        Sunday: scheduleDay
    }
    FavDishes: iMeal[]
    AdditionalServices: string[]
    SuitableCases: string[]
    AdminGUIDs: string[]
}

const Rest = () => {
    const [rest, setRest] = useState<iRest | null>(null)
    const [promotions, setPromotions] = useState<{ Count: number, Promotions: iRestPromotion[] }>({
        Count: 0,
        Promotions: []
    })
    const [reviews, setReviews] = useState<iReviews>({Count: 0, Reviews: []})
    const [events, setEvents] = useState<{ Count: number, Events: iRestEvent[] }>({Count: 0, Events: []})
    const [searchParams] = useSearchParams()
    const params = useParams()

    const establishmentId = params.id ? params.id : searchParams.get('id')
    const navigate = useNavigate();

    const [openNewReview, setOpenNewReview] = useState<boolean>(false);

    const fetchEstablishment = async () => {
        if (!establishmentId) {
            console.log('Wrong establishment id')
            return
        }

        await getEstablishment(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                switch (resp.status) {
                    case 200:
                        return resp.json()
                }
            }
            return null
        }).then(body => {
            if (body) {
                console.log(body)
                setRest({...body})
            } else {
                console.log('Wrong establishment id')
            }
            console.log(rest)
        })
        await getEstablishmentPromotions(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setPromotions(body)
            }
        })
        await getEstablishmentReviews(establishmentId).then(resp => {
            if (resp.ok) {
                console.log(resp)
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setReviews(body)
            }
        })
        await getEstablishmentEvents(establishmentId).then(resp => {
            console.log(resp)
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            if (body) {
                console.log(body)
                setEvents(body)
            }
        })
    }

    useEffect(() => {
        fetchEstablishment()
    }, [])


    if (rest == null) {
        return (<div>Загрузка...</div>)
    } else
        return (
            <>
                <RestHeader/>
                <Row>
                    <Col xs={5}>
                        {
                            <img src={rest.Images[0]} alt={''} style={{
                                height: 315,
                                borderRadius: 4,
                                width: '100%',
                                objectFit: 'cover',
                                marginTop: 32
                            }}/>
                        }
                    </Col>
                    <Col>
                        <div style={{display: 'flex', gap: 8, flexWrap: 'wrap', height: 315, marginTop: 32}}>
                            {
                                rest.Images.slice(1, rest.Images.length - 1).map((img, ind) => <img
                                    style={{height: 'calc(50% - 4px)', borderRadius: 4}} key={ind} src={img}
                                    alt={rest.EstablishmentTitle}/>)
                            }
                        </div>
                    </Col>
                </Row>

                {rest.FavDishes.length > 0 ?
                    <>
                        <Row>
                            <Col>
                                <div style={{
                                    marginTop: 64,
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'baseline'
                                }}>
                                    <h2 className={styles.heading}>Лучшие блюда</h2>
                                    <Button type={"text"} size={"L"}
                                            onClick={() => navigate(`/rest/${establishmentId}/menu`)}>Открыть
                                        меню</Button>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            {
                                rest.FavDishes.map((el, index) =>
                                    <Col xs={3} key={index}>
                                        <MealCard meal={el}/>
                                    </Col>
                                )
                            }
                        </Row>
                    </>
                    :
                    <></>}

                <Row style={{marginTop: 64}}>
                    <Col xs={6}>
                        <img src={rest.Images[rest.Images.length - 1]} className={styles.features_photo}
                             alt={rest.EstablishmentTitle}/>
                    </Col>
                    <Col xs={6}>
                        <div className={styles.features_container}>
                            <h2 className={styles.features_title}>Особенности заведения</h2>
                            <p>{rest.Description}</p>
                            <div className={styles.features}>
                                {
                                    rest.SuitableCases.concat(rest.AdditionalServices).map((el, ind) =>
                                        <div key={ind} className={styles.feature}>
                                            <div className={styles.sqare}/>
                                            <span>{el}</span>
                                        </div>)
                                }
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row style={{marginTop: 64}}>
                    <Col xs={5}>
                        <div>
                            <h2 className={styles.contacts_title}>Контакты</h2>
                            <div className={styles.contacts_list}>
                                <div className={styles.contacts_list_item}>
                                    <Icons icon={'geo'} className={styles.contacts_icon}/>
                                    <span>{rest.Address}</span>
                                </div>
                                <div className={styles.contacts_list_item}>
                                    <Icons icon={'time'} className={cn(styles.contacts_icon, styles.schedule_icon)}/>
                                    <div className={styles.schedule} style={{textWrap: 'nowrap'}}>
                                        <div className={styles.schedule_item}>
                                            <span>Пн</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Monday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Monday.OpenTime)}-${prettyTime(rest.Schedule.Monday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Вт</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Tuesday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Tuesday.OpenTime)}-${prettyTime(rest.Schedule.Tuesday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Ср</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Wednesday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Wednesday.OpenTime)}-${prettyTime(rest.Schedule.Wednesday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Чт</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Thursday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Thursday.OpenTime)}-${prettyTime(rest.Schedule.Thursday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Пт</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Friday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Friday.OpenTime)}-${prettyTime(rest.Schedule.Friday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Сб</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Saturday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Saturday.OpenTime)}-${prettyTime(rest.Schedule.Saturday.CloseTime)}`}</span>
                                        </div>
                                        <div className={styles.schedule_item}>
                                            <span>Вс</span>
                                            <span className={styles.schedule_dots}/>
                                            <span>{rest.Schedule.Sunday.RestDay ? 'Выходной' : `${prettyTime(rest.Schedule.Sunday.OpenTime)}-${prettyTime(rest.Schedule.Sunday.CloseTime)}`}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.contacts_list_item}>
                                    <Icons icon={'phone'} className={styles.contacts_icon}/>
                                    <span>{prettyPhone(rest.Contacts.MobilePhone)}</span>
                                </div>
                                <div className={styles.contacts_list_item}>
                                    <Icons icon={'internet'} className={styles.contacts_icon}/>
                                    <Link to={rest.Contacts.SiteURL}
                                          target={'_blank'}>{rest.Contacts.SiteURL}</Link>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col xs={7}>
                        <Map defaultState={{center: [rest.Geolocation.Latitude, rest.Geolocation.Longitude], zoom: 15}}
                             style={{width: '100%', height: 400, borderRadius: 4}} className={styles.ymap}>
                            <Placemark geometry={[rest.Geolocation.Latitude, rest.Geolocation.Longitude]}
                                       defaultOptions={{iconColor: '#CF3E00'}}/>
                            <ZoomControl/>
                        </Map>
                    </Col>
                </Row>


                <Row style={{marginTop: 64, display: 'flex', flexDirection: 'column', gap: '16px'}}>
                    <h2 className={styles.heading}>Рейтинг</h2>

                    <Row>
                        <div className={styles.rating_buttons}>
                            <div className={styles.rating}>
                                <p>{rest.Rating.AvgRating.toFixed(1)}</p>
                                <div className={styles.stars_reviews}>
                                    <Stars rating={rest.Rating.AvgRating} size={"l"}/>
                                    <p>{rest.Rating.ReviewCount} {switchEnding(['отзывов', 'отзыв', 'отзывов'], rest.Rating.ReviewCount)}</p>
                                </div>
                            </div>

                            <div className={styles.btns_reviews}>
                                <Button type={"secondary"} size={"M"}
                                        onClick={() => navigate(`/rest/${establishmentId}/reviews`)}>Посмотреть все
                                    отзывы</Button>
                                <Button type={"primary"} size={"M"} onClick={() => setOpenNewReview(true)}>Оставить
                                    отзыв</Button>
                            </div>
                        </div>
                    </Row>

                    <Row>
                        <div className={styles.rates}>
                            {Object.keys(rest.Rating.AvgRates).map((criterion, index) =>
                                // @ts-ignore
                                <RatingCriterion criterion={switchCriterion(criterion.toLowerCase())} rating={rest.Rating.AvgRates[criterion]} key={index} />)}
                        </div>
                    </Row>

                    <h3 className={styles.subtitle_reviews}>Отзывы</h3>

                    <Row>
                        {reviews.Reviews.slice(0, 3).map((r, ind) => <Review review={r} type={'rest'} onDelete={() => 0} key={ind} />)}
                    </Row>
                </Row>


                <Row style={{marginTop: 64}}>
                    {
                        promotions.Count > 0 ?
                            <>
                                <h2 className={styles.heading}>Акции</h2>
                                <Row>
                                    {
                                        promotions.Promotions.map((el, ind) =>
                                            <Col md={3}>
                                                <PromotionCard promotion={el}/>
                                            </Col>)

                                    }
                                </Row>
                            </>
                            :
                            <>
                            </>
                    }
                </Row>

                <Row style={{marginTop: 64}}>
                    {
                        events.Count > 0 ?
                            <>
                                <h2 className={styles.heading}>Мероприятия</h2>

                                <Row>
                                    {
                                        events.Events.map((el, ind) =>
                                            <Col md={6}>
                                                <EventCard event={el}/>
                                            </Col>
                                        )
                                    }
                                </Row>

                            </>
                            :
                            <>
                            </>
                    }
                </Row>
                <Modal open={openNewReview} setOpen={setOpenNewReview}>
                    <NewReview onComplete={fetchEstablishment}/>
                </Modal>
            </>
        )
}

export default Rest
