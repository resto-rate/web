import React, {useEffect, useRef, useState} from 'react';
import {Outlet} from "react-router-dom";
import {YMaps} from '@pbe/react-yandex-maps';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {getProfile} from "./api/auth";
import {initialUserContextState, iUser, UserContext} from "./context/userContext";
import {AlertContext, iAlert, initialAlertContextState} from "./context/alertContext";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Mobile from "./components/Mobile/Mobile";
import Alert from "./components/UIKit/Alert/Alert";
import {CollectionsContext, ICollection, initialCollectionsContextState} from "./context/CollectionsContext";
import {getCollections} from "./api/establishment";


function App() {
    const [user, setUser] = useState<iUser>(structuredClone(initialUserContextState.user));
    const [alert, setAlert] = useState<iAlert>(initialAlertContextState.alert);
    const [collections, setCollections] = useState(initialCollectionsContextState.collections);

    const timerRef = useRef(null);

    useEffect(() => {
        console.log('here')
        if (timerRef.current) {
            clearTimeout(timerRef.current);
        }
        // @ts-ignore
        timerRef.current = setTimeout(() => {
            setAlert(initialAlertContextState.alert);
        }, 5000);
    }, [alert]);

    useEffect(() => {
        const fetchUser = async () => {
            await getProfile(localUser.At.Token).then(resp => {
                console.log(resp)
                return resp.json()
            }).then(body => {
                console.log(body)
                console.log({...user, ...localUser, ...body, Empty: false})
                setUser({...user, ...localUser, ...structuredClone(body), Empty: false})
                console.log('User loaded')
            })
        }

        const localUser = JSON.parse(localStorage.getItem('user')!);
        if (localUser && localUser !== 'undefined') {
            fetchUser().catch(err => console.log(err))
        } else {
            const localCity = localStorage.getItem('city');
            if (localCity === null) {
                localStorage.setItem('city', 'Нижний Новгород')
                setUser({...user, City: 'Нижний Новгород'})
            } else if (user.Empty) {
                setUser({...user, City: localCity})
            }
        }
    }, []);

    useEffect(() => {
        if (user.Empty) {
            return
        }
        getCollections(user.AccountGUID).then(resp => {
            if (resp.ok) {
                return resp.json()
            }
        }).then(body => {
            console.log(body)
            setCollections(body)
        })
    }, [user]);

    if (window.screen.width <= 575) {
        return (
            <Mobile/>
        )
    }
    return (
        <YMaps>
            <UserContext.Provider value={{user: user, setUser: setUser}}>
                <CollectionsContext.Provider value={{collections: collections, setCollections: setCollections}}>
                    <AlertContext.Provider value={{alert: alert, setAlert: setAlert}}>
                        <Container style={{display: 'flex', flexDirection: 'column', minHeight: '100vh'}}>
                            <Row>
                                <Header/>
                            </Row>
                            <Outlet/>
                            <div style={{flex: 1}}/>
                            <Row>
                                <Footer/>
                            </Row>
                        </Container>
                        <Alert/>
                    </AlertContext.Provider>
                </CollectionsContext.Provider>
            </UserContext.Provider>
        </YMaps>
    )
}

export default App;
