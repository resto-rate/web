export const switchCriterion: (criterion: string) => 'Обслуживание' | 'Питание' | 'Атмосфера' | 'Цена - качество' | 'Время ожидания' = criterion => {
    switch (criterion) {
        case 'service':
            return 'Обслуживание'
        case 'vibe':
            return 'Атмосфера'
        case 'food':
            return 'Питание'
        case 'waitingtime':
            return 'Время ожидания'
        case 'pricequality':
            return 'Цена - качество'
        default:
            return 'Обслуживание'
    }
}

export const prettyPhone: (phone: string) => string = phone => {
    return '8 (' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6, 8) + '-' + phone.substring(8, 10)
}

export const prettyTime: (time: number) => string = time => {
    return new Date(time * 1000).toJSON().substring(11, 16)
}

export const openAuth = () => {
    //@ts-ignore
    document.getElementById('header_login').click()
}

export const capitalizeFirst = (str: string) => {
    return str[0].toUpperCase() + str.substring(1).toLowerCase()
}

export const copyCurrentHref = () => {
    navigator.clipboard.writeText(window.location.href)
}

export const timeToNumber = (time: string) => {
    return Number(time.substring(0, 2)) * 60 * 60 + Number(time.substring(3)) * 60
}

export const numberToTime = (number: number) => {
    const t = new Date(number)
    console.log(t.toISOString().substring(11, 16))
    return t.toISOString().substring(11, 16)
}

export const dateToNumber = (date: string) => {
    const newDate = new Date(date)
    console.log(newDate.getTime())
    return newDate.getTime() / 1000
}

export const numberToDate = (date: number) => {
    var t = new Date(date)
    console.log(t.toISOString().substring(0, 10))
    return t.toISOString().substring(0, 10)
}

export const switchEnding = (forms: string[], number: number) => {
    if (11 <= number % 100 && number % 100 <= 19 || 0 === number % 10 || 5 <= number % 10) {
        return forms[0]
    }
    if (2 <= number % 10 && number % 10 <= 5) {
        return forms[2]
    }
    return forms[1]
}

//export const

export const encodeURISearchParams = (params: object) => '&' + Object.entries(params).map(([key, value]) => `${key}=${encodeURI(value)}`).join('&')
